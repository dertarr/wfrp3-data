package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/mux"
	//"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

func (whdb *whDatabase) actionCardGet(w http.ResponseWriter, r *http.Request) {

	//vars := mux.Vars(r)
	w.WriteHeader(http.StatusOK)
	//fmt.Println(vars)
	//fmt.Println(r.URL.Query())
}

func (whdb *whDatabase) actionCardPost(w http.ResponseWriter, r *http.Request) {

	//vars := mux.Vars(r)
	w.WriteHeader(http.StatusOK)
}

func initRouter(whdb *whDatabase, port int, fileserverPath string) {

	r := mux.NewRouter()
	r.HandleFunc("/cardaction", whdb.actionCardGet).Methods("GET")
	r.HandleFunc("/cardaction", whdb.actionCardPost).Methods("POST")

	if fileserverPath == "" {
		selfpath, _ := os.Readlink("/proc/self/exe")
		fileserverPath = filepath.Dir(selfpath)
	}
	fs := http.StripPrefix("/", http.FileServer(http.Dir(fileserverPath)))
	r.PathPrefix("/").Handler(fs)

	http.ListenAndServe(fmt.Sprintf(":%d", port), r)
}
