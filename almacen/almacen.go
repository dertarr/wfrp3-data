package main

import (
	"flag"
	//"fmt"
	"os"

	"github.com/op/go-logging"
)

var log = logging.MustGetLogger("example")
var logformat = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} > %{level:.4s} %{id:04x}%{color:reset} %{message}`,
)

const (
	// DefaultPort indicates TCP port the REST API will listen by default
	DefaultPort = 8080
	// DefaultDbFilename DB filename
	DefaultDbFilename = "wfrp3-webkit.dat"
	// DefaultStaticPath defines path to static site web server
	DefaultStaticPath = ""
)

func main() {
	logbackend := logging.NewLogBackend(os.Stderr, "", 0)
	logbackendFormatter := logging.NewBackendFormatter(logbackend, logformat)
	logging.SetBackend(logbackendFormatter)

	var port int
	var dbFilename string
	var fileserverPath string
	flag.IntVar(&port, "p", DefaultPort, "HTTP port for the REST API to losten on")
	flag.StringVar(&dbFilename, "db", "", "Path to file with WFRP3 web toolkit database")
	flag.StringVar(&fileserverPath, "fs", "", "Root directory to the content for the fileserver")
	flag.Parse()

	whdb := initWhDatabase(dbFilename)
	log.Infof("Started WFRP3 webkit database handler on port %d", port)
	initRouter(whdb, port, fileserverPath)
	log.Critical("Exited WFRP3 webkit database handler unexpectedly")
}
