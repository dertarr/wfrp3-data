package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

type whDatabase struct {
	db *sqlx.DB
}

func initWhDatabase(filename string) *whDatabase {

	if filename == "" {
		selfpath, _ := os.Readlink("/proc/self/exe")
		filename = filepath.Join(filepath.Dir(selfpath), DefaultDbFilename)
	} else {
		dbDir := filepath.Dir(filename)
		if _, err := os.Stat(dbDir); os.IsNotExist(err) {
			if err = os.MkdirAll(dbDir, os.ModeDir); err != nil {
				log.Panicf("Cannot create database file '%s', error: %v", filename, err)
			}
		}
	}

	fmt.Println(filename)
	doInit := false
	stat, err := os.Stat(filename)
	if err != nil {
		if os.IsNotExist(err) {
			doInit = true
		} else {
			log.Panicf("Cannot create database file '%s', error: %v", filename, err)
		}
	} else if stat.Mode().IsDir() {
		log.Panicf("Specified path '%s' is a directory", filename)
	}

	db, err := sqlx.Connect("sqlite3", filename)
	if err != nil {
		log.Panicf("Cannot open database on path '%s', error: %v", filename, err)
	}
	if doInit {
		db.MustExec(wfrpSchema)
		log.Info("Created blank WFRP3 webkit database")
	}

	log.Infof("Initialized DB on path '%s'", filename)
	return &whDatabase{db: db}
}
