package whdbobj

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/op/go-logging"
	"github.com/stretchr/testify/require"
)

func getTestDB(t *testing.T) *sqlx.DB {
	db, err := sqlx.Connect("sqlite3", "whdbobj.dat.test")
	require.NoError(t, err, "Cannot create test database")
	return db
}

func getTestLogger(t *testing.T) *logging.Logger {
	var name = fmt.Sprintf("test") // Only to have "fmt" imported
	var log = logging.MustGetLogger(name)
	logging.SetLevel(logging.CRITICAL, name)
	return log
}

func assertCountCardSides(t *testing.T, db *sqlx.DB, expected int) {
	var clist []cardSide
	db.Select(&clist, "SELECT id FROM CardActionSide;")
	require.Equal(t, expected, len(clist))
}

func assertCardAction(t *testing.T, db *sqlx.DB, expected, inspected CardAction) {
	require.Equal(t, db, inspected.db)
	require.Equal(t, *expected.cons, *inspected.cons)
	require.Equal(t, *expected.reck, *inspected.reck)
	require.Equal(t, expected.data, inspected.data)
}

func newCard(db *sqlx.DB, unique string, sameReck bool) CardAction {

	cons := cardSide{ID: unique + ":cons", Name: "Name" + unique, NameAux: "NameAux" + unique,
		Tags: "Tags" + unique, Recharge: "Recharge" + unique, Difficulty: "Difficulty" + unique,
		CheckText: "CheckTest" + unique, Requirements: "Requirements" + unique, Effect: "Effect" + unique,
		Special:  "Special" + unique,
		MainText: fmt.Sprintf("8,%d:MainText%s", len(unique), unique),
		Options:  fmt.Sprintf("8,%d:opt1=123opt2=%s", len(unique)+5, unique),
	}
	reck := cons
	if sameReck == false {
		reck.ID = unique + ":reck"
		reck.Name = "Name" + unique + "Reck"
	}
	return CardAction{
		db:   db,
		cons: &cons,
		reck: &reck,
		data: cardActionData{DisplayName: "DisplayName" + unique, Lngcode: "en",
			Cardtype: "support", Defence: false, Source: "core", ConsID: cons.ID, ReckID: reck.ID}}
}

func TestMarshalStringSlice(t *testing.T) {

	srcslice := []string{"Line1", "Line2", "", "LineLineLine3", ""}
	marshalled := marshalStringSlice(srcslice)
	require.Equal(t, "5,5,0,13,0:Line1Line2LineLineLine3", marshalled)
	unmarshalled, err := unmarshalStringSlice(marshalled)
	require.NoError(t, err)
	require.Equal(t, fmt.Sprintf("%v", srcslice), fmt.Sprintf("%v", unmarshalled))

	_, err = unmarshalStringSlice("12,34,56SomeString")
	require.EqualError(t, err, errUnmarshal.Error())
	_, err = unmarshalStringSlice("4,z:SomeString")
	require.EqualError(t, err, errUnmarshal.Error())
	_, err = unmarshalStringSlice("4,5:SomeString")
	require.EqualError(t, err, errUnmarshal.Error())
	_, err = unmarshalStringSlice("4,7:SomeString")
	require.EqualError(t, err, errUnmarshal.Error())
}

func TestCardActionDBRW(t *testing.T) {

	db := getTestDB(t)
	InitSchema(db)
	log := getTestLogger(t)

	// Create an action card and store it to DB - should be no errors
	card1 := newCard(db, "c1", false)
	require.NoError(t, card1.Insert())

	// Make sure that both card sides are in
	assertCountCardSides(t, db, 2)
	require.Equal(t, card1.cons, getCardActionSide(db, log, card1.cons.ID))
	require.Equal(t, card1.reck, getCardActionSide(db, log, card1.reck.ID))

	// Read all cards present - should be only one
	dbcards := GetAllCardAction(db, log)
	require.Equal(t, 1, len(dbcards))
	assertCardAction(t, db, card1, *dbcards[0])

	// Create another action card with equal sides
	card2 := newCard(db, "c2", true)
	require.NoError(t, card2.Insert())

	// Make sure that all three sides are present
	assertCountCardSides(t, db, 3)
	require.Equal(t, card1.cons, getCardActionSide(db, log, card1.cons.ID))
	require.Equal(t, card1.reck, getCardActionSide(db, log, card1.reck.ID))
	require.Equal(t, card2.cons, getCardActionSide(db, log, card2.cons.ID))

	// Read all cards present - should be two
	dbcards2 := GetAllCardAction(db, log)
	require.Equal(t, 2, len(dbcards2))
	assertCardAction(t, db, card1, *dbcards2[0])
	assertCardAction(t, db, card2, *dbcards2[1])

	// Find the card1
	fc1 := FindCardAction(db, log, "DisplayNamec1", "en", "core")
	require.True(t, fc1 != nil && fc1.cons != nil && fc1.reck != nil)
	require.Equal(t, db, fc1.db)
	require.Equal(t, "DisplayNamec1", fc1.data.DisplayName)
	require.Equal(t, "c1:cons", fc1.cons.ID)
	require.Equal(t, "c1:reck", fc1.reck.ID)
}

func TestEncodeCardSide(t *testing.T) {

	// Test with normal card side
	card1 := newCard(nil, "c1", true)
	jside, err := encodeCardSide(card1.cons)
	require.NoError(t, err)
	mmm := make(map[string]interface{})
	require.NoError(t, json.Unmarshal([]byte(jside), &mmm))
	require.Equal(t, 9, len(mmm))
	require.Equal(t, "Namec1", mmm["name"])
	require.Equal(t, "NameAuxc1", mmm["name_aux"])
	require.Equal(t, "Tagsc1", mmm["tags"])
	require.Equal(t, "Rechargec1", mmm["recharge"])
	require.Equal(t, "Difficultyc1", mmm["difficulty"])
	require.Equal(t, "CheckTestc1", mmm["check"])
	require.Equal(t, "Requirementsc1", mmm["requirements"])
	require.Equal(t, []interface{}{"opt1=123", "opt2=c1"}, mmm["options"])
	mmmeffect, _ := mmm["effect"].(map[string]interface{})
	require.Equal(t, 3, len(mmmeffect))
	require.Equal(t, "Effectc1", mmmeffect["effect"])
	require.Equal(t, "Specialc1", mmmeffect["special"])
	require.Equal(t, []interface{}{"MainText", "c1"}, mmmeffect["text"])

	// Test with invalid options
	card1.cons.Options = "15:invalid"
	_, err = encodeCardSide(card1.cons)
	require.EqualError(t, err, errUnmarshal.Error())

	// Test with invalid main text
	card1.cons.Options = "10:opt1=valid"
	card1.cons.MainText = "15:invalid"
	_, err = encodeCardSide(card1.cons)
	require.EqualError(t, err, errUnmarshal.Error())

	// Test completely empty cardside to make sure omitempty directive works
	emptyside := &cardSide{}
	jempty, err := encodeCardSide(emptyside)
	require.NoError(t, err)
	eee := make(map[string]interface{})
	require.NoError(t, json.Unmarshal([]byte(jempty), &eee))
	require.Equal(t, 3, len(eee))
	require.Equal(t, "", eee["name"])
	require.Equal(t, "", eee["recharge"])
	eeeeffect, _ := eee["effect"].(map[string]interface{})
	require.Equal(t, 0, len(eeeeffect))
}

func TestCardIntoJson(t *testing.T) {

	// Normal card with equal sides
	card1 := newCard(nil, "c1", true)
	jside, _ := encodeCardSide(card1.cons)
	jcard, err := card1.IntoJson()
	require.NoError(t, err)
	exp := fmt.Sprintf(`{"fmtver":"1.0","name":"DisplayNamec1","type":"support","lngcode":"en",`+
		`"source":"core","cons":%s,"reck":true}`, jside)
	require.Equal(t, exp, jcard)

	// Defence card with different sides
	card2 := newCard(nil, "c2", false)
	card2.data.Defence = true
	jcons, _ := encodeCardSide(card2.cons)
	jreck, _ := encodeCardSide(card2.reck)
	jcard2, err := card2.IntoJson()
	require.NoError(t, err)
	exp2 := fmt.Sprintf(`{"fmtver":"1.0","name":"DisplayNamec2","type":"defence_support",`+
		`"lngcode":"en","source":"core","cons":%s,"reck":%s}`, jcons, jreck)
	require.Equal(t, exp2, jcard2)

	// Error on converting side
	card2.reck.Options = "15:invalid"
	_, err = card2.IntoJson()
	require.EqualError(t, err, errUnmarshal.Error())
}

func TestDecodeCardSide(t *testing.T) {

	// Test well formed JSON card side
	jsonside1 := `{
		"name": "SomeCard Name",		"name_aux": "SomeCard Aux Name",
		"tags": "Ongoing, Slayer",		"recharge": "x",
		"difficulty": "[W6k][W6k]",		"check": "Weapon Skill (ST) vs. Target Defence",
		"requirements": "Engaged with the target, melee weapon equipped",
		"effect": {
			"effect": "You swing your weapon and damage the target",
			"special": "If you have an ally engaged with yourself, add [W6w] to the dice pool",
			"text": ["[w1] You hit the target with normal damage",
				     "[w1] [w1] [w1] You hit the target with +2 damage"
				    ]
		},
		"options": ["effect_size=0.95"]
	}`
	side1, err := decodeCardSide([]byte(jsonside1))
	require.NoError(t, err)
	require.Equal(t, "SomeCard Name", side1.Name)
	require.Equal(t, "SomeCard Aux Name", side1.NameAux)
	require.Equal(t, "Ongoing, Slayer", side1.Tags)
	require.Equal(t, "x", side1.Recharge)
	require.Equal(t, "[W6k][W6k]", side1.Difficulty)
	require.Equal(t, "Weapon Skill (ST) vs. Target Defence", side1.CheckText)
	require.Equal(t, "Engaged with the target, melee weapon equipped", side1.Requirements)
	require.Equal(t, "You swing your weapon and damage the target", side1.Effect)
	require.Equal(t, "If you have an ally engaged with yourself", side1.Special[:41])
	require.Equal(t, "42,48:[w1]", side1.MainText[:10])
	require.Equal(t, "16:effect_size=0.95", side1.Options)

	// Test with error during JSON unmarshal ("text" property of effect is not a string slice)
	jsonside2 := `{
		"name": "SomeCard Name",		"name_aux": "SomeCard Aux Name",
		"tags": "Ongoing, Slayer",		"recharge": "x",
		"difficulty": "[W6k][W6k]",		"check": "Weapon Skill (ST) vs. Target Defence",
		"requirements": "Engaged with the target, melee weapon equipped",
		"effect": {
			"effect": "You swing your weapon and damage the target",
			"special": "If you have an ally engaged with yourself, add [W6w] to the dice pool",
			"text": "[w1] You hit the target with normal damage"
		},
		"options": ["effect_size=0.95"]
	}`
	_, err = decodeCardSide([]byte(jsonside2))
	require.EqualError(t, err, "json: cannot unmarshal string into Go value of type []string")

	// Missing required "recharge" property
	jsonside3 := strings.Replace(jsonside1, `"recharge": "x",`, "", 1)
	_, err = decodeCardSide([]byte(jsonside3))
	require.EqualError(t, err, errMissing.Error())

	// Missing entire "effect" section
	jsonside4 := `{"name":"SomeCard Name","name_aux":"SomeCard Aux Name","tags":"Ongoing, Slayer",
		"recharge":"x","difficulty":"[W6k][W6k]","check":"Weapon Skill (ST) vs. Target Defence",
		"requirements":"Engaged with the target, melee weapon equipped","options":["effect_size=0.95"]}`
	_, err = decodeCardSide([]byte(jsonside4))
	require.EqualError(t, err, errMissing.Error())
}

func TestCardFromJson(t *testing.T) {

	db := getTestDB(t)

	jsonside1 := `{"name":"SomeCard Name","name_aux":"SomeCard Aux Name","tags":"Ongoing, Slayer",` +
		`"recharge":"x","difficulty":"[W6k][W6k]","check":"Weapon Skill (ST) vs. Target Defence",` +
		`"requirements":"Melee weapon equipped","options":["effect_size=0.95"],` +
		`"effect":{"effect":"Eff","special":"Spec","text":["[w1]","[w1][w1][w1]"]}}`
	jsonside2 := `{"name":"SomeCard Name \"with\": backslash and colon","name_aux":"SomeCard Aux Name",` +
		`"tags":"Ongoing, Slayer","recharge":"x","difficulty":"[W6k][W6k][W6k]",` +
		`"check":"Weapon Skill (ST) vs. Target Defence",` +
		`"requirements":"Melee weapon equipped","effect":{"effect":"Eff"}}`

	// A card with different cons and reck sides
	jsoncard1 := `{"fmtver":"1.0","name":"Card Display Name","type":"blessing","lngcode":"es","source":"core"` +
		`,"cons":` + jsonside1 + `,"reck":` + jsonside2 + `}`
	card1, err := NewCardAction(db, []byte(jsoncard1))
	require.NoError(t, err)
	require.Equal(t, card1.data.DisplayName, "Card Display Name")
	require.Equal(t, card1.data.Cardtype, "blessing")
	require.Equal(t, card1.data.Lngcode, "es")
	require.Equal(t, card1.data.Source, "core")
	require.Equal(t, card1.data.Defence, false)
	require.Equal(t, card1.cons.Name, "SomeCard Name") // For sides only check the names in this test
	require.Equal(t, card1.reck.Name, `SomeCard Name "with": backslash and colon`)

	// Another card with cons = reck and defence = true
	jsoncard2 := `{"fmtver":"1.0","name":"Card Display Name","type":"defence_blessing","lngcode":"es","source":"core"` +
		`,"cons":` + jsonside1 + `,"reck":true}`
	card2, err := NewCardAction(db, []byte(jsoncard2))
	require.NoError(t, err)
	require.Equal(t, card2.data.DisplayName, "Card Display Name")
	require.Equal(t, card2.data.Cardtype, "blessing")
	require.Equal(t, card2.data.Lngcode, "es")
	require.Equal(t, card2.data.Source, "core")
	require.Equal(t, card2.data.Defence, true)
	require.Equal(t, card2.cons.Name, "SomeCard Name") // For sides only check the names in this test
	require.Equal(t, card2.reck.Name, "SomeCard Name")

	// A card with not supported version
	jsoncard3 := strings.Replace(jsoncard2, "1.0", "0.7beta", 1)
	_, err = NewCardAction(db, []byte(jsoncard3))
	require.EqualError(t, err, errVersion.Error())

	// A card with invalid JSON structure
	jsoncard4 := strings.Replace(jsoncard2, `"type":`, `"type":[`, 1)
	_, err = NewCardAction(db, []byte(jsoncard4))
	require.EqualError(t, err, "invalid character ':' after array element")

	// A card with missing required "type" field
	jsoncard5 := strings.Replace(jsoncard2, `"type":"defence_blessing",`, "", 1)
	_, err = NewCardAction(db, []byte(jsoncard5))
	require.EqualError(t, err, errMissing.Error())

	// A card with missing required "reck" section
	jsoncard6 := `{"fmtver":"1.0","name":"Card Display Name","type":"blessing","lngcode":"es","source":"core"` +
		`,"cons":` + jsonside1 + `}`
	_, err = NewCardAction(db, []byte(jsoncard6))
	require.EqualError(t, err, errMissing.Error())

	// A card with invalid "cons" structure
	jsoncard7 := `{"fmtver":"1.0","name":"Card Display Name","type":"blessing","lngcode":"es","source":"core"` +
		`,"cons":{},"reck":true}`
	_, err = NewCardAction(db, []byte(jsoncard7))
	require.EqualError(t, err, errMissing.Error())

	// A card with invalid "reck" structure
	jsoncard8 := `{"fmtver":"1.0","name":"Card Display Name","type":"blessing","lngcode":"es","source":"core"` +
		`,"cons":` + jsonside1 + `,"reck":{}}`
	_, err = NewCardAction(db, []byte(jsoncard8))
	require.EqualError(t, err, errMissing.Error())
}
