package whdbobj

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/op/go-logging"
)

const (
	formatVersion = "1.0"
)

var (
	sideProps = []string{"id", "name", "name_aux", "tags", "recharge", "difficulty", "check_text",
		"requirements", "effect", "special", "main_text", "options"}
	cardProps = []string{"name", "lngcode", "cardtype", "defence", "source", "cons", "reck"}

	insertCmdSide = fmt.Sprintf("INSERT INTO CardActionSide (%s) VALUES (:%s)",
		strings.Join(sideProps, ","), strings.Join(sideProps, ",:"))
	insertCmdCard = fmt.Sprintf("INSERT INTO CardAction (%s) VALUES (:%s)",
		strings.Join(cardProps, ","), strings.Join(cardProps, ",:"))

	errUnmarshal = fmt.Errorf("marshalled string is corrupted")
	errVersion   = fmt.Errorf("source JSON string should have version %s", formatVersion)
	errMissing   = fmt.Errorf("source JSON card action misses one of the required properties")
)

var dbinitCardAction = `
BEGIN TRANSACTION;
DROP TABLE IF EXISTS CardAction;
CREATE TABLE CardAction (
	name			VARCHAR(64)		NOT NULL,
	lngcode			VARCHAR			NOT NULL,
	cardtype   		VARCHAR			NOT NULL,
	defence			BOOLEAN			NOT NULL DEFAULT 0,
	source			VARCHAR			NOT NULL,
	cons			VARCHAR(16)		NOT NULL REFERENCES CardActionSide(id),
	reck			VARCHAR(16)		NOT NULL REFERENCES CardActionSide(id),
	PRIMARY KEY 	(name, lngcode, source)
);
COMMIT;`

var dbinitCardActionSide = `
BEGIN TRANSACTION;
DROP TABLE IF EXISTS CardActionSide;
CREATE TABLE CardActionSide (
	id				VARCHAR			NOT NULL PRIMARY KEY,
	name			VARCHAR(64)		NOT NULL,
	name_aux		VARCHAR(64),
	tags			VARCHAR(64),
	recharge   		VARCHAR			NOT NULL,
	difficulty		VARCHAR,
	check_text		VARCHAR,
	requirements	VARCHAR,
	effect			VARCHAR,
	special			VARCHAR,
	main_text		VARCHAR,
	options			VARCHAR
);
COMMIT;`

// Encode slice of strings to a single string for storing in DB
func marshalStringSlice(slice []string) string {

	lengths := make([]string, 0)
	for _, item := range slice {
		lengths = append(lengths, strconv.Itoa(len(item)))
	}
	return fmt.Sprintf("%s:%s", strings.Join(lengths, ","), strings.Join(slice, ""))
}

// Decode string from DB to a slice of Strings
func unmarshalStringSlice(src string) ([]string, error) {

	if src == "" {
		return make([]string, 0), nil
	}

	srcdiv := strings.SplitN(src, ":", 2)
	if len(srcdiv) != 2 {
		return nil, errUnmarshal
	}

	pos := 0
	output := make([]string, 0)
	lengths := strings.Split(srcdiv[0], ",")
	for _, ls := range lengths {
		li, err := strconv.Atoi(ls)
		if err != nil || pos+li > len(srcdiv[1]) {
			return nil, errUnmarshal
		}
		output = append(output, srcdiv[1][pos:pos+li])
		pos += li
	}
	if pos != len(srcdiv[1]) {
		return nil, errUnmarshal
	}
	return output, nil
}

type cardSide struct {
	ID           string `db:"id"`
	Name         string
	NameAux      string `db:"name_aux"`
	Tags         string
	Recharge     string
	Difficulty   string
	CheckText    string `db:"check_text"`
	Requirements string
	Effect       string
	Special      string
	MainText     string `db:"main_text"`
	Options      string
}

type cardActionData struct {
	DisplayName string `db:"name"`
	Lngcode     string
	Cardtype    string
	Defence     bool
	Source      string
	ConsID      string `db:"cons"`
	ReckID      string `db:"reck"`
}

// CardAction ...
type CardAction struct {
	db         *sqlx.DB
	data       cardActionData
	cons, reck *cardSide
}

func InitSchema(db *sqlx.DB) {

	db.MustExec(dbinitCardAction)
	db.MustExec(dbinitCardActionSide)
}

func NewCardAction(db *sqlx.DB, jsondata []byte) (*CardAction, error) {

	ca := &CardAction{db: db}
	if jsondata != nil {
		if err := ca.FromJson(jsondata); err != nil {
			return nil, err
		}
	}

	return ca, nil
}

func getCardActionSide(db *sqlx.DB, log *logging.Logger, id string) *cardSide {

	var cardside cardSide
	query := "SELECT * FROM CardActionSide WHERE id=$1;"
	if err := db.Get(&cardside, query, id); err != nil {
		log.Error("Error execuring query: '%s'", query)
		return nil
	}
	return &cardside
}

func SelectCardAction(db *sqlx.DB, log *logging.Logger, params map[string]string) []*CardAction {
	return nil
}

func FindCardAction(db *sqlx.DB, log *logging.Logger, displayName, lngcode, source string) *CardAction {

	ca := CardAction{db: db}
	query := "SELECT * FROM CardAction WHERE name=$1 AND lngcode=$2 AND source=$3;"
	if err := db.Get(&ca.data, query, displayName, lngcode, source); err != nil {
		log.Error("Error execuring query: '%s'", query)
		return nil
	}

	ca.cons = getCardActionSide(db, log, ca.data.ConsID)
	if ca.data.ConsID == ca.data.ReckID {
		ca.reck = ca.cons
	} else {
		ca.reck = getCardActionSide(db, log, ca.data.ReckID)
	}

	if ca.cons == nil || ca.reck == nil {
		return nil
	}
	return &ca
}

func GetAllCardAction(db *sqlx.DB, log *logging.Logger) []*CardAction {

	var cards []*CardAction
	var cardsdata []cardActionData
	query := "SELECT * FROM CardAction ORDER BY name ASC;"
	if err := db.Select(&cardsdata, query); err != nil {
		log.Error("Error execuring query: '%s'", query)
		return cards
	}
	for _, cdata := range cardsdata {
		ca := CardAction{db: db, data: cdata}
		ca.cons = getCardActionSide(db, log, cdata.ConsID)
		if cdata.ConsID != cdata.ReckID {
			ca.reck = getCardActionSide(db, log, cdata.ReckID)
		} else {
			ca.reck = ca.cons
		}
		cards = append(cards, &ca)
	}

	return cards
}

func (ca *CardAction) Insert() error {

	tx := ca.db.MustBegin()
	if _, err := tx.NamedExec(insertCmdSide, ca.cons); err != nil {
		tx.Rollback()
		return err
	}
	if *ca.reck != *ca.cons {
		if _, err := tx.NamedExec(insertCmdSide, ca.reck); err != nil {
			tx.Rollback()
			return err
		}
	}
	if _, err := tx.NamedExec(insertCmdCard, ca.data); err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()

	return nil
}

func (ca *CardAction) Delete() error {
	return nil
}

type jsonEffect struct {
	Effect   string   `json:"effect,omitempty"`
	Special  string   `json:"special,omitempty"`
	MainText []string `json:"text,omitempty"`
}

type jsonSide struct {
	Name         string     `json:"name"`
	NameAux      string     `json:"name_aux,omitempty"`
	Tags         string     `json:"tags,omitempty"`
	Recharge     string     `json:"recharge"`
	Difficulty   string     `json:"difficulty,omitempty"`
	CheckText    string     `json:"check,omitempty"`
	Requirements string     `json:"requirements,omitempty"`
	Effect       jsonEffect `json:"effect"`
	Options      []string   `json:"options,omitempty"`
}

func encodeCardSide(side *cardSide) (string, error) {

	text, err := unmarshalStringSlice(side.MainText)
	if err != nil {
		return "", err
	}
	opt, err := unmarshalStringSlice(side.Options)
	if err != nil {
		return "", err
	}

	eff := jsonEffect{
		Effect:   side.Effect,
		Special:  side.Special,
		MainText: text,
	}

	frmside := jsonSide{
		Name:         side.Name,
		NameAux:      side.NameAux,
		Tags:         side.Tags,
		Recharge:     side.Recharge,
		Difficulty:   side.Difficulty,
		CheckText:    side.CheckText,
		Requirements: side.Requirements,
		Effect:       eff,
		Options:      opt,
	}

	sideOutput, _ := json.Marshal(frmside)
	return string(sideOutput), nil
}

func decodeCardSide(cardside []byte) (*cardSide, error) {

	jside := jsonSide{}
	if err := json.Unmarshal([]byte(cardside), &jside); err != nil {
		return nil, err
	}
	if jside.Name == "" || jside.Recharge == "" {
		return nil, errMissing
	}
	if jside.Effect.Effect == "" && jside.Effect.Special == "" && len(jside.Effect.MainText) == 0 {
		return nil, errMissing
	}

	cside := cardSide{
		Name:         jside.Name,
		NameAux:      jside.NameAux,
		Tags:         jside.Tags,
		Recharge:     jside.Recharge,
		Difficulty:   jside.Difficulty,
		CheckText:    jside.CheckText,
		Requirements: jside.Requirements,
		Effect:       jside.Effect.Effect,
		Special:      jside.Effect.Special,
		MainText:     marshalStringSlice(jside.Effect.MainText),
		Options:      marshalStringSlice(jside.Options),
	}
	return &cside, nil
}

// IntoJson converts CardAction into a JSON formatted string
func (ca *CardAction) IntoJson() (string, error) {

	cons, err := encodeCardSide(ca.cons)
	if err != nil {
		return "", err
	}

	reck := "true"
	if *ca.cons != *ca.reck {
		reck, err = encodeCardSide(ca.reck)
		if err != nil {
			return "", err
		}
	}

	defmap := map[bool]string{false: "", true: "defence_"}
	output := []string{
		fmt.Sprintf("\"fmtver\":\"%s\"", formatVersion),
		fmt.Sprintf("\"name\":\"%s\"", ca.data.DisplayName),
		fmt.Sprintf("\"type\":\"%s%s\"", defmap[ca.data.Defence], ca.data.Cardtype),
		fmt.Sprintf("\"lngcode\":\"%s\"", ca.data.Lngcode),
		fmt.Sprintf("\"source\":\"%s\"", ca.data.Source),
		fmt.Sprintf("\"cons\":%s", cons),
		fmt.Sprintf("\"reck\":%s", reck),
	}

	return fmt.Sprintf("{%s}", strings.Join(output, ",")), nil
}

type jsonDecodeCard struct {
	Fmtver      string          `json:"fmtver"`
	DisplayName string          `json:"name"`
	Cardtype    string          `json:"type"`
	Lngcode     string          `json:"lngcode"`
	Source      string          `json:"source"`
	Cons        json.RawMessage `json:"cons"`
	Reck        json.RawMessage `json:"reck"`
}

// FromJson fills CardAction props based on JSON-formatted input string
func (ca *CardAction) FromJson(jsondata []byte) error {

	var err error
	dec := &jsonDecodeCard{}
	if err = json.Unmarshal(jsondata, dec); err != nil {
		return err
	}
	if dec.Fmtver != formatVersion {
		return errVersion
	}
	if dec.DisplayName == "" || dec.Cardtype == "" || dec.Lngcode == "" || dec.Source == "" ||
		len(dec.Cons) == 0 || len(dec.Reck) == 0 {
		return errMissing
	}

	ca.data.DisplayName = dec.DisplayName
	ca.data.Cardtype = strings.TrimPrefix(dec.Cardtype, "defence_")
	ca.data.Lngcode = dec.Lngcode
	ca.data.Defence = strings.HasPrefix(dec.Cardtype, "defence_")
	ca.data.Source = dec.Source

	// Decode conservative and reckless side data
	if ca.cons, err = decodeCardSide(dec.Cons); err != nil {
		return err
	}
	if len(dec.Reck) == 4 && string(dec.Reck) == "true" {
		ca.reck = ca.cons
	} else {
		if ca.reck, err = decodeCardSide(dec.Reck); err != nil {
			return err
		}
	}

	return nil
}
