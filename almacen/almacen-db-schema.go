package main

const wfrpSchema = `
-- SQLite schema for WFRP3 web toolkit
BEGIN TRANSACTION;

-- Users of the application
DROP TABLE IF EXISTS user;
CREATE TABLE user (
	id				VARCHAR(16)		NOT NULL PRIMARY KEY,
	login			VARCHAR			NOT NULL,
	pwdhash			VARCHAR(32)		NOT NULL,
	name			VARCHAR,
	role			VARCHAR,
	comment			VARCHAR
);

COMMIT;
`
