// jshint esversion: 6

// Map for character codes in [wx?] to the actual WFRP 3ed symbols.
const whSymbolMap = {"1": "\u0501", // Success
                     "2": "\u0502", // Boon
                     "3": "\u0503", // Sigmar's Comet
                     "4": "\u0504", // Bane
                     "5": "\u0505", // Chaos Star
                     "6": "\u0507", // Black d6
                     "7": "\u0506", // White d6
                     "8": "\u0509", // Black d8
                     "0": "\u0511", // Black d10
                     "x": "\u0500", // Challenge
                     "d": "\u0520", // Delay
                     "e": "\u0514", // Extortion
                     "h": "\u0521", // Left stance puzzle
                     "j": "\u0522", // Middle stance puzzle
                     "k": "\u0523", // Right stance puzzle
                     "r": "\u0519", // Rightseous success
                     "f": "\u0512", // Fatigue
                     "s": "\u0513", // Stress
                     "w": "\u0515", // Wound
};
const whSymbols = ["1", "2", "3", "4", "5", "6", "7", "8", "0",
                   "x", "d", "e", "h", "j", "k", "r", "f", "s", "w"];

// Map for colors in [wx?] to color names to be inserted into <font> section.
// TODO: Add support for glow outline expressed in capital character of a color.
const whColourMap = {"b": "blue",  "p": "purple", "y": "yellow", "n": "grey",
                     "w": "white", "r": "red",    "g": "green",  "k": "black"};
const whStrokeMap = {"b": true,    "p": true,     "y": "black",  "n": true,
                     "w": "black", "r": true,     "g": true,     "k": "white"};
const whColours = ["b", "p", "y", "o", "w", "r", "g", "k"];

// Function replaces WFRP 3ed code with unicode symbols and its colour
function whReplaceChar(tx) {
  var newchar = whSymbolMap[tx[2]];
  var newcolor = (tx[3] == "]") ? null : whColourMap[tx[3]];
  var newstroke = (tx[1] == 'W') ? whStrokeMap[tx[3]] : null;
  return [newchar, newcolor, newstroke];
}

// Convert hash one character keys to a string to be used in regex
function combineKeys(theHash) {
  var a = [];
  for (let i in theHash) a.push(i);
  return a.join('');
}

// Regex part to match character codes and color codes
const whSymbolRegx = "[" + combineKeys(whSymbolMap) + "]";
const whColourRegx = "[" + combineKeys(whColourMap) + "]";
const whCombiRegx = "\\[[Ww]" + whSymbolRegx + whColourRegx + "?\\]";
const whSeparators = [
    whCombiRegx,       // WFRP 3ed symbols
    "\\[b\\]", "\\[\\/b\\]", // Bold
    "\\[i\\]", "\\[\\/i\\]", // Italic
    " ", "\\[t\\]", "\\[n\\]", "\t", "\n", // Tabs and new lines
    "\\[\\*\\]", "\\[\\>\\]", // Bullets
    "\\[\\-\\]" // Short dash
];

// This regex will detect following sequences:
const whRegex = new RegExp("(" + whSeparators.join('|') + ")");
const whExactRegex = new RegExp("^" + whCombiRegx + "$");
const whFirstWordRegex = new RegExp("^(.+?) ");

// Split line of text to words and separators based on whSeparators regexp
function whSplit(text) {
  var splits = [];

  while (text.length > 0) {
    let res = text.match(whRegex);
    if (res === null) {
      splits.push(text);
      break;
    }
    if (res.index > 0) {
      splits.push(text.slice(0, res.index));
    }
    splits.push(res[0]);
    text = text.slice(res.index + res[0].length);
  }
  return splits;
}


// It is highly recommended to measure text drawn at 360 dpi and then scale the width to real size
function measureText(text, font) {

  ctxMeasure.font = font;
  text = text.replace(/ /g, '\u00a0');
  return ctxMeasure.measureText(text).width;
}

// prepareMultiText draws WFRP 3ed formatted text over a rectangle space.
// Function returns actual height of the text drawn.
function prepareMultiText(ctx, text, adjuster, params, offsetY) {

  // Doubled parameters for drawing
  var interval = adjuster.space == "small" ? 0 : params.interval;
  var fs = params.fontsize * adjuster.size,
      x = params.x,
      y = (params.y + offsetY),
      w = params.w,
      h = params.h,
      dy = fs + interval,
      extra = params.extra || 0;

  var bold = "",
      italic = "";

  ctx.lineWidth = params.strokesize;
  ctx.lineJoin = 'round';
  var font = bold + italic + fs + "px Wfrp3Lib";

  var spacelen = measureText("'", font);
  var lines = [];
  var currentLine = [];
  var currentLineWidth = 0;

  // Gather the lines into a struct
  var splits = whSplit(text);
  for (let tx of splits) {
    let colour = null;
    let stroke = null;

    if (whExactRegex.test(tx)) {
      let res = whReplaceChar(tx);
      tx = res[0];
      colour = res[1];
      stroke = (res[2] === true) ? params.whstroke : res[2];
    } else if (tx == "[b]" || tx == "[/b]") {
      bold = (tx[1] == "/") ? "" : "bold ";
      continue;
    } else if (tx == "[i]" || tx == "[/i]") {
      italic = (tx[1] == "/") ? "" : "italic ";
      continue;
    } else if (tx == "[n]" || tx == "\n") {
      // TODO: fix blank space after new line
      lines.push({'items': currentLine, 'width': currentLineWidth});
      currentLine = [];
      currentLineWidth = 0;
      continue;
    } else if (tx == "[t]" || tx == "\t") {
      // TODO: fix tab spacing
      // TODO: fix blank space after tab
      let space = Math.floor((currentLineWidth + 4.1*spacelen) / (4*spacelen)) * (4*spacelen);
      currentLine.push({'text': "", 'font': font, 'stroke': null, 'width': space});
      currentLineWidth += space;
      continue;
    } else if (tx == "[*]") {
      tx = "\u16EB";
    } else if (tx == "[>]") {
      tx = "\u203A";
    } else if (tx == "[-]") {
      tx = "\u2013";
    }

    // Add the element to array of the text line elements
    font = bold + italic + fs + "px Wfrp3Lib";
    let lastElem = (currentLine.length > 0) ? currentLine[currentLine.length - 1] : null;
    if (lastElem && lastElem.font == font && stroke === null && lastElem.stroke === null) {
      var newtext = lastElem.text + tx;
      let twid = measureText(newtext, font);
      if (currentLineWidth - lastElem.width + twid <= w) {
        currentLineWidth = currentLineWidth - lastElem.width + twid;
        lastElem.text = newtext;
        lastElem.width = twid;
        continue;
      }
    }

    let twid = measureText(tx, font);
    let sx = (stroke === null) ? 0 : ctx.lineWidth;
    if ((currentLineWidth + twid + sx) > w) {
      lines.push({'items': currentLine, 'width': currentLineWidth});
      currentLine = [];
      currentLineWidth = 0;
      // Do not add leading space to the beginning of next line
      if (tx == " ") continue;
    }
    currentLine.push({'text': tx,
                      'font': font,
                      'stroke': stroke,
                      'colour': (colour === null) ? params.fontcolor : colour,
                      'width': twid + sx
                     });
    currentLineWidth = currentLineWidth + twid + sx + extra;

  }

  // Push the last line if needed
  if (currentLine.length > 0) {
    lines.push({'items': currentLine, 'width': currentLineWidth});
  }

  // Define starting position based on vertical alignment
  let posy = 0;
  let textBaseline = "top";
  let totalw = lines.length > 1 ? (lines.length - 1) * dy + fs : lines.length * fs;
  switch(params.align_y) {
    case "middle":
      if (totalw < h) {
        posy = h/2 - totalw/2;
        textBaseline = "middle";
      }
      break;
    case "bottom":
      if (totalw < h) {
        posy = h;
        dy = -dy;
        lines.reverse();
        textBaseline = "bottom";
      }
      break;
  }
  ctx.textBaseline = textBaseline;

  // Draw the lines one by one
  for (let ln of lines) {
    let curline = ln.items,
        curwidth = ln.width,
        posx = 0;

    switch(params.align_x) {
      case "center":
        posx = w/2 - curwidth/2;
        break;
      case "right":
        posx = w - curwidth;
        break;
    }

    for (let item of curline) {
      ctx.font = item.font;
      if (item.stroke !== null) {
        posx = posx + ctx.lineWidth/2;
        ctx.strokeStyle = item.stroke;
        ctx.strokeText(item.text, x + posx, y + posy);
      }
      ctx.fillStyle = item.colour;
      ctx.fillText(item.text, x + posx, y + posy);
      posx = posx + item.width + extra;
      if (item.stroke !== null) {
        posx = posx - ctx.lineWidth/2;
      }
    }
    posy = posy + dy;
    if (posy > h) {
      break;
    }
  }

  return lines.length > 0 ? fs + (lines.length - 1) * dy : 0;
}

function prepareText(ctx, item, params, offsetY) {

  // Doubled parameters for drawing
  var fs = params.fontsize * item.size,
      x = params.x,
      y = params.y + offsetY,
      w = params.w,
      h = params.h,
      ffx = Number(params.font_factor_x) || 1,
      alignX = params.align_x,
      alignY = params.align_y;
  // Doubled parameters for drawing
  // var fs = params.fontsize * item.size * 2,
  //     x = params.x * 2,
  //     y = (params.y + offsetY) * 2,
  //     w = params.w * 2,
  //     h = params.h * 2,
  //     ffx = Number(params.font_factor_x) || 1,
  //     alignX = params.align_x,
  //     alignY = params.align_y;

  ctx.font = [params.fontmod || "", fs + "px", params.font].join(" ");

  var posx = 0, posy = 0;
  var twid = measureText(item.text, ctx.font) * ffx;

  // If the text does not fit to width then compress the canvas by width
  ctx.save();
  ffx = (twid > w) ? ffx * w / twid : ffx;

  ctx.scale(ffx, 1);

  switch(alignX) {
    case "left":
      posx = x/ffx;
      ctx.textAlign = "left";
      break;
    case "center":
      posx = (x + w/2) / ffx;
      ctx.textAlign = "center";
      break;
    case "right":
      posx = (x + w) / ffx;
      ctx.textAlign = "right";
      break;
    default: console.log("Invalid horisontal alignment.");
  }
  switch(alignY) {
    case "top":
      posy = y;
      ctx.textBaseline = "top";
      break;
    case "middle":
      posy = y + h/2;
      ctx.textBaseline = "middle";
      break;
    case "bottom":
      posy = y + h;
      ctx.textBaseline = "bottom";
      break;
    default: console.log("Invalid vertical alignment.");
  }

  if ("stroke" in params && params.stroke !== null) {
    ctx.strokeStyle = params.stroke;
    ctx.lineWidth = params.strokesize * item.size;
    ctx.lineJoin = 'round';
    ctx.strokeText(item.text, posx, posy);
  }

  ctx.fillStyle = params.fontcolor;
  ctx.fillText(item.text, posx, posy);
  ctx.restore();
}

// getItemByKey takes an action card and retrieves a value by a given name.
// Action cards have two sides: conservative and reckless. Conservative side
// is considered "main". Card's file name would be equivalent to the name of the
// card in conservative stance.
// Often some (or even all) attributes on the reckless side would copy the ones
// from the conservative side. In that case corresponding attribute on the
// reckless side would be equal to a boolean "true" - to save space and effort.
function getItemByKey(objCard, itemKey, isReckless) {

  if (itemKey in objCard) {
    return objCard[itemKey];
  }

  var objCons = objCard.conservative;
  var item = objCons[itemKey] || null;

  if (isReckless) {
    var itemReck = objCard.reckless[itemKey] || null;
    item = (itemReck === true) ? item : itemReck;
  }

  return item;
}

// addIcon adds an image for card based on key
function addIcon(ctx, cardData, key, map) {

  var value = cardData[key];

  if (value !== null) {
    var image = map[value];
    var x = globActionCardStyle[key].x,
        y = globActionCardStyle[key].y,
        w = globActionCardStyle[key].w,
        fw = image.width,
        fh = image.height;
    var posx = x + (w - fw)/2;

    ctx.drawImage(image, 0, 0, image.width, image.height,
                         Math.round(posx), Math.round(y), fw, fh);
  }
}

function canvRoundRectangle(ctx, x, y, w, h, r) {

  x = Math.round(x);
  y = Math.round(y);
  w = Math.round(w);
  h = Math.round(h);
  r = Math.round(r);

  ctx.beginPath();
  ctx.moveTo(x + r, y);
  ctx.lineTo(x + w - r, y);
  ctx.quadraticCurveTo(x + w, y, x + w, y + r);
  ctx.lineTo(x + w, y + h - r);
  ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
  ctx.lineTo(x + r, y + h);
  ctx.quadraticCurveTo(x, y + h, x, y + h - r);
  ctx.lineTo(x, y + r);
  ctx.quadraticCurveTo(x, y, x + r, y);
}

function canvUpperRoundRectangle(ctx, x, y, w, h, r) {

  x = Math.round(x);
  y = Math.round(y);
  w = Math.round(w);
  h = Math.round(h);
  r = Math.round(r);

  ctx.beginPath();
  ctx.moveTo(x + r, y);
  ctx.lineTo(x + w - r, y);
  ctx.quadraticCurveTo(x + w, y, x + w, y + r);
  ctx.lineTo(x + w, y + h);
  ctx.lineTo(x, y + h);
  ctx.lineTo(x, y + r);
  ctx.quadraticCurveTo(x, y, x + r, y);
}

function canvFillRoundRectangle(ctx, x, y, w, h, r, color) {
    canvRoundRectangle(ctx, x, y, w, h, r);
    ctx.fillStyle = color;
    ctx.fill();
}

function canvFillUpperRoundRectangle(ctx, x, y, w, h, r, color) {
    canvUpperRoundRectangle(ctx, x, y, w, h, r);
    ctx.fillStyle = color;
    ctx.fill();
}

function canvStrokeRoundRectangle(ctx, x, y, w, h, r, lw, color) {

    x = x + lw/2; y = y + lw/2; w = w - lw; h = h - lw; r = r - lw;

    canvRoundRectangle(ctx, x, y, w, h, r);
    ctx.lineWidth = lw;
    ctx.strokeStyle = color;
    ctx.stroke();
}

// drawCardSide creates a canvas for a 2.25" x 3,5" card at 600 dpi,
// draws card's content on that canvas and returnes it to the caller.
function drawCardSide(drawCtx, isReckless) {

  var sideData = (isReckless) ? globCardData.reck : globCardData.cons;
  // Create two context - one for drawing headers and another for normal text
  // Somehow the headers with stroke look better on a double-size canvas, while
  // normal text look better on a normal size canvas with positions rounded to integer.
  var style = globActionCardStyle;
  //var dpr = window.devicePixelRatio || 1;
  var dpr = 2;
  var textCanv = document.createElement('canvas');
  var headCanv = document.createElement('canvas');
  textCanv.width = style.width * dpr; textCanv.height = style.height * dpr;
  headCanv.width = style.width * dpr; headCanv.height = style.height * dpr;
  var textCtx = textCanv.getContext('2d');
  var headCtx = headCanv.getContext('2d');
  textCtx.setTransform(dpr, 0, 0, dpr, 0, 0);
  headCtx.setTransform(dpr, 0, 0, dpr, 0, 0);

  var enableCornerRadius = document.getElementById("enable_corner_radius");
  var borderRadius = (enableCornerRadius.checked)
      ? globActionCardStyle.border_radius
      : 0;

  // Draw the shape of the cards with background fonts
  canvFillRoundRectangle(drawCtx, 0, 0, style.width, style.height,
                                  borderRadius, style.bgcolor);
  var fillHeader = sideData.isReckless ? style.reck_color : style.cons_color;
  canvFillUpperRoundRectangle(drawCtx, 0, 0, style.width, style.header_height,
                              borderRadius, fillHeader);
  drawCtx.beginPath();
  drawCtx.fillStyle = "#402010";
  drawCtx.fillRect(0, style.header_height, style.width, style.check_height);
  // Add card type and recharge
  addIcon(drawCtx, globCardData, "type", imgActionTypeMap);
  addIcon(drawCtx, sideData, "recharge", imgRechargeMap);

  // Draw simple text layers
  var layers = ["name", "name_aux", "tags", "check"];
  for (let tx of layers) {
    let item = sideData[tx];
    if (item.text !== "") {
      prepareText(headCtx, item, style[tx], 0);
    }
  }

  // Draw difficulty modification section
  let dfc = sideData.difficulty;
  if (dfc.text !== "") {
    let adjuster = {size: dfc.size};
    prepareMultiText(textCtx, dfc.text, adjuster, style.difficulty, 0);
  }

  var offset = style.effect_offset;

  // Draw image
  if (sideData.image !== null) {
    var imgoffset = style.check.y + style.check.h
    drawCtx.drawImage(sideData.image.img, 0, imgoffset, style.image.w, style.image.h);
    offset = offset + style.image.h;
    if (style.requirements.separator) {
      let sep = style.requirements.separator;
      drawCtx.beginPath();
      drawCtx.fillStyle = sep.color;
      drawCtx.fillRect(0, imgoffset + style.image.h, style.width, sep.h);
      offset = offset + sep.h;
    }
  }

  // Draw requirements
  let req = sideData.requirements;
  if (req.text !== "") {
    let bg = style.requirements.background;
    let adjuster = {size: req.size, space: req.space};
    let yy = prepareMultiText(textCtx, req.text, adjuster, style.requirements, offset);
    canvFillRoundRectangle(drawCtx, bg.x, offset - bg.dy,
                            bg.w, yy + 2*bg.dy, bg.radius, bg.color);
    offset = offset + yy + style.requirements.dy;
    if (style.requirements.separator) {
      let sep = style.requirements.separator;
      drawCtx.beginPath();
      drawCtx.fillStyle = sep.color;
      drawCtx.fillRect(0, offset, style.width, sep.h);
      offset = offset + sep.h + style.requirements.dy;
    }
  }

  // Draw effect section
  var needbg = false,
      bg = style.effect.background,
      effect = sideData.effect;
  var adjuster = {size: effect.size, space: effect.space};
  var dy = style.effect.dy[effect.dy] * effect.size;

  var extra = ["special", "effect"];
  for (let itemKey of extra) {
    let text = effect[itemKey];
    if (text !== "") {
      text = text.replace(whFirstWordRegex, "[b]$&[/b]");
      let yy = prepareMultiText(textCtx, text, adjuster, style.effect, offset);
      if (needbg) {
        canvFillRoundRectangle(drawCtx, bg.x, offset - bg.dy,
                                        bg.w, yy + 2*bg.dy,
                                        bg.radius, bg.color);
      }
      needbg = !needbg;
      offset = offset + yy + dy;
    }
  }

  if (effect.text) {
    var startbg = offset - bg.dy;
    var widthbg = 2*bg.dy - dy; // "-dy" is to neutralize first line Y delta
    var alltext = effect.text;

    for (let i = 0; i < alltext.length; i++) {
      let text = alltext[i];
      let nexttext = (alltext[i+1] || "\a\a\a\a").substring(0, 3);
      let yy = prepareMultiText(textCtx, text, adjuster, style.effect, offset);
      widthbg = widthbg + yy + dy;

      if (text.substring(0, 3) != nexttext) {
        if (needbg) {
          canvFillRoundRectangle(drawCtx, bg.x, startbg, bg.w, widthbg,
                                          bg.radius, bg.color);
        }
        needbg = !needbg;
        startbg = offset + yy + dy - bg.dy;
        widthbg = 2*bg.dy - dy;
      }
      offset = offset + yy + dy;
    }
  }

  // Merge draw layer with text layers - text layer on the top
  drawCtx.drawImage(headCanv, 0, 0, style.width, style.height);
  drawCtx.drawImage(textCanv, 0, 0, style.width, style.height);

  var enableThickBorder = document.getElementById("enable_thick_border");
  var hbw = (enableThickBorder.checked) ? style.hbw * 2 : style.hbw;

  // Draw border for the card side
  canvStrokeRoundRectangle(drawCtx, 0, 0, style.width, style.height,
                                    borderRadius, hbw, "black");
}
