// jshint esversion: 6

function convertPath(urlPath) {
  var path = "#";
  var items = urlPath.split('/');
  if ((items.length > 2) && (items[0] == "act")) {
    if (items[1] == "ru") {
      return "cards/core_ru/actions/" + items[2] + "_ru";
    } else if (items[1] == "dertarr_ru") {
      return "cards/dertarr_ru/actions/" + items[2] + "_ru";
    } else if (items[1] == "dertarr") {
      return "cards/dertarr/actions/" + items[2];
    } else if (items[1] == "en") {
      return "cards/core/actions/" + items[2];
    }

  }

  return "#";
}

// validatePath makes sure the path argument is safe {
function validatePath(cardPath) {

  if (cardPath[0] == "/" || cardPath[0] == "\\") {
      console.log("URL argument should not start with a slash or a backslash.");
      return false;
  }
  if (cardPath.endsWith(".json")) {
      console.log("URL argument should not include 'json' extension.");
      return false;
  }
  if (cardPath.includes("../") || cardPath.includes("..\\")) {
      console.log("URL argument is not allowed to backtrace.");
      return false;
  }
  return true;
}

function whRenderCard(urlPath) {
  var cardPath = convertPath(urlPath);
  if (cardPath != "#" && validatePath(cardPath)) {
    var client = new XMLHttpRequest();
    var fullPath = cardPath + ".json?nocache=" + (new Date()).getTime();
    client.open('GET', fullPath);
    client.onreadystatechange = function() {
        if (client.readyState === XMLHttpRequest.DONE && client.status === 200) {
          jsonToCardAction(client.responseText);
          window.onresize = function() {
            repositionMainLook();
            cardToEditor();
          };
        }
      };
    client.send();
  }
  return false;
}

function whRefClicked(e) {
  const params = new URLSearchParams(e.getAttribute("href"));
  const act = params.get('act');
  if (act === null) {
    drawEmptyCard();
  } else {
    whRenderCard(act);
  }
}

// This file holds global declarations of pre-loaded things.
window.onload = function() {

  function loadImageMap(srcDirPath, dstHashMap, commonNamePart) {
    for (var key in dstHashMap) {
      if (dstHashMap.hasOwnProperty(key)) {
        let imgPath = srcDirPath + commonNamePart + key + ".png";
        dstHashMap[key] = pxloader.addImage(imgPath);
      }
    }
  }

  function whDownloadCanvasPng() {
    renderWarhammerCard(null, globCardData);
    this.download = globCardData.genFileName() + " 360dpi.png";
    this.href = canv360.toDataURL('image/png');
  }

  function whDownloadCanvasJpeg() {
    renderWarhammerCard(null, globCardData);
    this.download = globCardData.genFileName() + " 360dpi.jpg";
    this.href = canv360.toDataURL('image/jpeg', 1);
  }

  function whExportJson() {
    var jsonData = globCardData.toJSON();
    this.download = globCardData.genFileName() + ".json";
    this.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(jsonData);
  }

  function whImportJson(e) {
    var file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = function(e) {
        jsonToCardAction(e.target.result);
      };
      reader.readAsText(file);
    }
  }

  function whRedrawCard(e) {
    let whCanvas = document.getElementById("wh_canvas");
    renderWarhammerCard(whCanvas, globCardData);
  }

  var pxloader = new PxLoader();
  loadImageMap("icons/", imgRechargeMap, "recharge_");
  loadImageMap("icons/", imgActionTypeMap, "action_");

  // callback that will be run once images are ready
  pxloader.addCompletionListener(function() {

    const params = new URLSearchParams(window.location.search);
    const act = params.get('act');

    if (act === null) {
      drawEmptyCard();
    } else {
      whRenderCard(act);
    }

    var whDownloadLnk = document.getElementById("download_360dpi_jpeg");
    whDownloadLnk.addEventListener('click', whDownloadCanvasJpeg, false);

    var whExportLnk = document.getElementById("export_json");
    var whDownloadLnk = document.getElementById("download_360dpi_png");
    whDownloadLnk.addEventListener('click', whDownloadCanvasPng, false);

    var whExportLnk = document.getElementById("export_json");
    whExportLnk.addEventListener('click', whExportJson, false);

    var whImportFile = document.getElementById("import_json");
    whImportFile.addEventListener('change', whImportJson, false);

    var whCardRadius = document.getElementById("enable_corner_radius");
    whCardRadius.addEventListener('change', whRedrawCard, false);

    var whCardRadius = document.getElementById("enable_thick_border");
    whCardRadius.addEventListener('change', whRedrawCard, false);
  });

  // begin downloading images
  pxloader.start();

  repositionMainLook(redraw=false);
};
