// jshint esversion: 6
// jshint laxbreak: true
// jshint loopfunc: true

globIsReckless = false;
globIsEffect = false;
globIsCommon = true;

function cardToEditor() {

  function checkRadio(name, value) {
    $(`.btn-${name}`).removeClass("active");
    let rbtn = $(`input:radio[name=inp-${name}][value=${value}]`);
    rbtn.prop("checked", true);
    rbtn.parent().addClass("active");
  }

  let srcSide = globIsReckless ? globCardData.reck : globCardData.cons;
  let itemsInput = ["name", "name_aux", "tags", "check", "difficulty", "requirements"];
  for (let item of itemsInput) {
    $(`#inp_${item}`).val(srcSide[item].text);
    $(`#txtsz_${item}`).text(srcSide[item].size.toFixed(2));
  }
  checkRadio("type", globCardData.type);
  checkRadio("recharge", srcSide.recharge);

  checkRadio("effect-dy", srcSide.effect.dy);
  checkRadio("effect-space", srcSide.effect.space);
  $('#txtsz_effect').text(srcSide.effect.size.toFixed(2));
  $('#inp_effect_effect').val(srcSide.effect.effect || "");
  $('#inp_effect_special').val(srcSide.effect.special || "");
  $('#inp_effect_text').val((srcSide.effect.text || [""]).join("\n"));
  $('#inp_image_name').text(srcSide.image === null ? "No image selected" : srcSide.image.name);

  $('#inp_display_name').val(globCardData.displayName);
  $('#inp_source').val(globCardData.source);
  $('#inp_lngcode').val(globCardData.lngcode);

  autosize.update($('.text-autosize'));

  return false;
}

function editorToCard() {
  let dstSide = globIsReckless ? globCardData.reck : globCardData.cons;
  let itemsInput = ["name", "name_aux", "tags", "check", "difficulty", "requirements"];
  for (let item of itemsInput) {
    dstSide[item].text = $(`#inp_${item}`).val();
    dstSide[item].size = Number($(`#txtsz_${item}`).text());
  }
  dstSide.recharge = document.querySelector('input[name="inp-recharge"]:checked').value;
  globCardData.type = document.querySelector('input[name="inp-type"]:checked').value;
  globCardData.displayName = $('#inp_display_name').val();
  globCardData.source = $('#inp_source').val();
  globCardData.lngcode = $('#inp_lngcode').val();

  dstSide.effect.dy = document.querySelector('input[name="inp-effect-dy"]:checked').value;
  dstSide.effect.space = document.querySelector('input[name="inp-effect-space"]:checked').value;
  dstSide.effect.size = Number($('#txtsz_effect').text());
  dstSide.effect.effect = $('#inp_effect_effect').val();
  dstSide.effect.special = $('#inp_effect_special').val();
  let txt = $('#inp_effect_text').val();
  dstSide.effect.text = txt !== "" ? txt.split("\n") : [];

  let whCanvas = document.getElementById("wh_canvas");
  renderWarhammerCard(whCanvas, globCardData);
}

activeEditor = null;

function insertIntoEditor(btnval) {
  if (activeEditor !== null) {
    if (btnval == "b" || btnval == "i") {
      return insertStyleIntoEditor(btnval);
    }
    let val = activeEditor.value,
        pos = activeEditor.selectionStart;
    if ($(activeEditor).attr("id") != "inp_difficulty") {
      if (pos > 0 && val[pos-1] != " " && val[pos-1] != "\n") btnval = " " + btnval;
      if (pos < val.length && val[pos] != " " && val[pos] != "\n") btnval = btnval + " ";
    }
    activeEditor.value = val.substring(0, pos) + btnval + val.substring(pos, val.length);
    activeEditor.selectionStart =  activeEditor.selectionEnd = pos + btnval.length;
    autosize.update(activeEditor);
    editorToCard();
  }
  return false;
}

function insertStyleIntoEditor(style) {

  let val = activeEditor.value,
      pos = activeEditor.selectionStart,
      end = activeEditor.selectionEnd;
  if (end > pos) {
    let open = "[" + style + "]",
        close = "[/" + style + "]",
        sub = open + val.substring(pos, end) + close;
    activeEditor.value = val.substring(0, pos) + sub + val.substring(end, val.length);
    activeEditor.selectionStart = activeEditor.selectionEnd = end + 7;
  } else {
    let pre = val.substring(0, pos);
    let open = new RegExp("\\[" + style + "\\]", "g"),
        close = new RegExp("\\[\\/" + style + "\\]", "g"),
        txt = (pre.match(open) || []).length > (pre.match(close) || []).length ? "[/" : "[";
    activeEditor.value = pre + txt + style + "]" + val.substring(pos, val.length);
    activeEditor.selectionStart = activeEditor.selectionEnd = pos + txt.length + 2;
  }
  autosize.update(activeEditor);
  editorToCard();
  return false;
}


const ButtonsResult = ['1', '2', '3', '4', '5', 'd', 'e', 'x', 'r', 'f', 's', 'w'];
const ButtonsDices = ["6w", "6k", "6y", "8b", "8p", "0g", "0r", "hr", "jn", "kg", "6r", "6b"];

function sizesControls(id) {
  return `<div style="white-space: nowrap">
            ${labelWithTooltip("Size", tt_size)}&nbsp;<span class="ddn-text" id="txtsz_${id}">1.00</span>
            <div class="btn-group">
              <label class="btn btn-info btn-outline btn-sz" id="btnsz_more_${id}">&#9650;</label>
              <label class="btn btn-info btn-outline btn-sz" id="btnsz_less_${id}">&#9660;</label>
            </div></div>`;
}

// public method for encoding an Uint8Array to base64
function encode64(input) {
  var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
  var output = "";
  var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
  var i = 0;

  while (i < input.length) {
    chr1 = input[i++];
    chr2 = i < input.length ? input[i++] : Number.NaN; // Not sure if the index
    chr3 = i < input.length ? input[i++] : Number.NaN; // checks are needed here

    enc1 = chr1 >> 2;
    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
    enc4 = chr3 & 63;

    if (isNaN(chr2)) {
        enc3 = enc4 = 64;
    } else if (isNaN(chr3)) {
        enc4 = 64;
    }
    output += keyStr.charAt(enc1) + keyStr.charAt(enc2) +
              keyStr.charAt(enc3) + keyStr.charAt(enc4);
  }
  return output;
}

function createEditorControls() {

  // Empty the current content of the editor items
  let smallControls = $('#editorParent').width() < 600;

  $('#editorSelectors, #divWhsButtons, #editActionTbl, #editActionEffectTbl').empty();

  var tblMeta = document.getElementById('editActionTblMeta'),
      tblMain = document.getElementById('editActionTbl'),
      tblEffect = document.getElementById('editActionEffectTbl');

  function appendSelectors(tbody) {

    let txt0 = smallControls ? "Common" : "Common<br>section",
        txt1 = smallControls ? "Main Data" : "Conservative<br>Main&nbsp;section",
        txt2 = smallControls ? "Effect" : "Conservative<br>Effect&nbsp;section",
        txt3 = smallControls ? "Main Data" : "Reckless<br>Main&nbsp;section",
        txt4 = smallControls ? "Effect" : "Reckless<br>Effect&nbsp;section";
    let active0 = globIsCommon ? "active" : "";
    let active1 = !globIsReckless && !globIsEffect && !globIsCommon ? "active" : "";
    let active2 = !globIsReckless && globIsEffect && !globIsCommon ? "active" : "";
    let active3 = globIsReckless && !globIsEffect && !globIsCommon ? "active" : "";
    let active4 = globIsReckless && globIsEffect && !globIsCommon ? "active" : "";
    let div = document.getElementById("editorSelectors");
    div.innerHTML =
      `<label class="btn btn-primary btn-outline btn-padding ${active0}">
         <input type="radio" name="inp_selector" value="common" autocomplete="off">${txt0}</input></label>
       <label class="btn btn-success btn-outline btn-padding ${active1}">
         <input type="radio" name="inp_selector" value="cons_main" autocomplete="off">${txt1}</input></label>
       <label class="btn btn-success btn-outline btn-padding ${active2}">
         <input type="radio" name="inp_selector" value="cons_effect" autocomplete="off">${txt2}</input></label>
       <label class="btn btn-danger btn-outline btn-padding ${active3}">
         <input type="radio" name="inp_selector" value="reck_main" autocomplete="off">${txt3}</input></label>
       <label class="btn btn-danger btn-outline btn-padding ${active4}">
         <input type="radio" name="inp_selector" value="reck_effect" autocomplete="off">${txt4}</input> </label>`;

    div.onchange = function() {
      let val = document.querySelector('input[name="inp_selector"]:checked').value;
      globIsReckless = (val == "reck_main" || val == "reck_effect");
      globIsEffect = (val == "cons_effect" || val == "reck_effect");
      globIsCommon = (val == "common");

      $("#div_table_meta").addClass("hidden");
      $("#div_table_main").addClass("hidden");
      $("#div_table_effect").addClass("hidden");
      if (val === "common") $("#div_table_meta").removeClass("hidden");
      if (val === "cons_main" || val === "reck_main") $("#div_table_main").removeClass("hidden");
      if (val === "cons_effect" || val === "reck_effect") $("#div_table_effect").removeClass("hidden");

      $('.btn-copy').attr("disabled", globIsReckless);
      $('.btn-copy').toggleClass("disable-a-href", globIsReckless);
      cardToEditor();
    };
  }

  function appendButtons() {

    let divButtons = document.getElementById("divWhsButtons");
    let whsSmall = smallControls ? "btn-whs-small " : "";

    function getDivButtons(extraClass) {
      let html = "";
      let div = document.createElement('div');
      for (let key of ButtonsResult) {
        let val = extraClass === "" ? `[w${key}]` : `[W${key}k]`;
        html +=`<a href="#" class="btn btn-default btn-whs disable-a-href ${whsSmall}${extraClass}"
                   disabled tabstop="-1" val="${val}">${whSymbolMap[key]}</a>`;
      }
      let key = extraClass === "" ? "b" : "i";
      let txt = extraClass === "" ? "<b>&nbsp;bold&nbsp;</b>" : "<i>italic</i>";
      html += `<a href="#" class="btn btn-default btn-whs disable-a-href ${whsSmall}"
                  style="font-family: monospace" disabled val="${key}">${txt}</a>`;
      div.className = "btn-group btn-group-full-width btn-whs-group";
      div.innerHTML = html;
      return div;
    }

    function getDivDiceButtons() {
      let html = "";
      let div = document.createElement('div');
      for (let key of ButtonsDices) {
        let val = `[W${key}]`;
        let glowClass = key[1] == 'k' ? "btn-whs-white-glow" : "btn-whs-black-glow";
        html += `<a href="#" class="btn btn-default btn-whs disable-a-href ${whsSmall}${glowClass}"
                    disabled style="-webkit-text-fill-color: ${whColourMap[key[1]]}"
                    val="${val}">${whSymbolMap[key[0]]}</a>`;
      }
      div.className = "btn-group btn-group-full-width btn-whs-group";
      div.innerHTML = html;
      return div;
    }

    divButtons.appendChild(getDivButtons(""));
    divButtons.appendChild(getDivButtons("btn-whs-glow"));
    divButtons.appendChild(getDivDiceButtons());
    $(".btn-whs").mouseup(function(){ $(this).blur(); return false; });
    $(".btn-whs").click(function(){ return insertIntoEditor( $(this).attr("val") ); });
  }

  function appendMeta(tbody, label, id, tooltip, tabindex) {
    let tr1 = tbody.insertRow(-1);
    tr1.innerHTML =
      `<td style="padding: 2px; width: 25%; tbindex=-1 vertical-align: middle">${labelWithTooltip(label, tooltip)}</td>
       <td style="padding: 2px; width: 75%; vertical-align: middle; font-family: monospace">
           <textarea class="form-control text-autosize" id="inp_${id}" tabindex=${tabindex} rows="1" minrows="1"
                     style="resize: none; padding: 2px 6px; line-height: 120%"></textarea></td>`;
    tbody.insertRow(-1).innerHTML = '<td style="padding: 1px; background-color: black" colspan="2"></td>';
  }

  function appendCtrl(tbody, label, id, tooltip, tabindex) {

    if (smallControls) {
      let tr1 = tbody.insertRow(-1),
          tr2 = tbody.insertRow(-1);
      tr1.innerHTML =
        `<td style="padding: 2px; vertical-align: middle; tabindex=-1 line-height: 110%">${labelWithTooltip(label, tooltip)}</td>
         <td style="padding: 2px; vertical-align: middle; text-align: center">${sizesControls(id)}</td>
         <td style="padding: 2px; vertical-align: middle">
           <button id="btn_${id}_copy" class="btn btn-block btn-danger btn-copy btn-padding">\u27A4</button></td>`;
      tr2.innerHTML =
        `<td style="padding: 2px; vertical-align: middle; font-family: monospace" colspan="3">
           <textarea class="form-control text-autosize wh-controls-small" tabindex=${tabindex} id="inp_${id}" rows="1" minrows="1"
                     style="resize: none; padding: 2px 6px; line-height: 120%"></textarea></td>`;
      tbody.insertRow(-1).innerHTML = '<td style="padding: 1px; background-color: black" colspan="3"></td>';
    } else {
      let tr = tbody.insertRow(-1);
      tr.innerHTML =
        `<td style="padding: 2px; width: 20%; vertical-align: middle; tabindex=-1 line-height: 110%">${labelWithTooltip(label, tooltip)}</td>
         <td style="padding: 2px; width: 50%; vertical-align: middle; font-family: monospace" colspan="2">
           <textarea class="form-control text-autosize" id="inp_${id}" rows="1" minrows="1" tabindex=${tabindex}
                     style="resize: none; padding: 2px 6px; line-height: 120%"></textarea></td>
         <td style="padding: 2px; vertical-align: middle; text-align: center">${sizesControls(id)}</td>
         <td style="padding: 2px; width: 5%; vertical-align: middle">
           <button id="btn_${id}_copy" class="btn btn-block btn-danger btn-copy">\u27A4</button></td>`;
      tbody.insertRow(-1).innerHTML = '<td style="padding: 1px; background-color: black" colspan="5"></td>';
    }

    $(`#btn_${id}_copy`).click(function() {
      globCardData.reck[id].text = globCardData.cons[id].text;
      globCardData.reck[id].size = globCardData.cons[id].size;
      editorToCard();
    });
  }

  function appendActionType(tbody, map) {

    let tr = tbody.insertRow(-1),
        td1 = tr.insertCell(-1),
        div = td1.appendChild(document.createElement("div")),
        div1 = div.appendChild(document.createElement("div")),
        div2 = div.appendChild(document.createElement("div"));

    td1.setAttribute("colspan", "2");
    td1.style = "padding: 2px; vertical-align: middle";
    div.setAttribute("data-toggle", "buttons");
    div1.className = div2.className = "btn-group btn-group-full-width";
    for (let key of Object.keys(map)) {
      let lbl = document.createElement("label");
      let small = smallControls ? "wh-controls-small" : "";
      lbl.className = `btn btn-padding btn-multiline btn-primary btn-outline btn-type ${small}`;
      lbl.innerHTML = `<input type="radio" name="inp-type" value="${key}">${map[key]}`;
      (key.includes("defence") ? div2 : div1).appendChild(lbl);
    }
    tbody.insertRow(-1).innerHTML = `<td style="padding: 1px; background-color: black" colspan="2"></td>`;

    tr.onchange = function() { editorToCard(); };
  }

  function appendRecharge(tbody, map, tooltip) {
    let tr = tbody.insertRow(-1);

    let htmlDiv1 = "",
        htmlDiv2 = "";
    for (let key of Object.keys(map)) {
      let html = `<label class="btn btn-padding btn-primary btn-outline btn-recharge">
                  <input type="radio" name="inp-recharge" value="${key}">${map[key]}</input></label>`;
      if (key == "s" || key == "x") htmlDiv2 += html; else htmlDiv1 += html;
    }

    let colspan = smallControls ? 1 : 3;
    let copyClass = smallControls ? "btn-padding" : "";
    tr.innerHTML = `
      <td style="padding: 2px; vertical-align: middle; line-height: 110%">${labelWithTooltip("Recharge", tooltip)}</td>
      <td style="padding: 2px; vertical-align: middle" colspan="${colspan}"><div data-toggle="buttons">
        <div class="btn-group btn-group-full-width">${htmlDiv1}</div>
        <div class="btn-group btn-group-full-width">${htmlDiv2}</div></div></td>
      <td style="padding: 2px; width: 5%; vertical-align: middle">
        <button id="btn_recharge_copy" class="btn btn-block btn-danger btn-copy ${copyClass}">\u27A4</button></td>`;
    tbody.insertRow(-1).innerHTML = `<td style="padding: 1px; background-color: black" colspan="${colspan+2}"></td>`;
    tr.onchange = function() { editorToCard(); };

    $(`#btn_recharge_copy`).click(function() {
      globCardData.reck.recharge = globCardData.cons.recharge;
      editorToCard();
    });
  }

  function appendImage(tbody, tooltip) {

    const imgW = globActionCardStyle.image.w;
    const imgH = globActionCardStyle.image.h;

    if (smallControls) {
      let tr1 = tbody.insertRow(-1),
          tr2 = tbody.insertRow(-1);
      tr1.innerHTML =
        `<td style="padding: 2px; vertical-align: middle; line-height: 110%">
             ${labelWithTooltip("Card image", tooltip)} ${imgW}&nbsp;&times;&nbsp;${imgH}&nbsp;px</td>
         <td style="padding: 2px; vertical-align: middle; text-align: center">
           <label id="btn_image_discard" class="btn btn-block btn-warning btn-outline btn-padding">Discard</label>
         <td style="padding: 2px; width: 5%; vertical-align: middle">
           <button id="btn_image_copy" class="btn btn-block btn-danger btn-copy">\u27A4</button></td>`;
      tr2.innerHTML =
        `<td style="padding: 2px; width: 15%; vertical-align: middle; overflow-x: hidden">
           <label id="inp_image_btn" class="btn btn-block btn-default">Image File</label>
           <input type="file" id="inp_image" accept="image/*" style="display: none"/></td>
         <td style="padding: 2px; vertical-align: middle; text-align: left" colspan="2">
           <div id="inp_image_name" style="display: inline-block; vertical-align: middle"></div>`;
    } else {
      let tr = tbody.insertRow(-1);
      tr.innerHTML =
        `<td style="padding: 2px; vertical-align: middle; line-height: 110%">
             ${labelWithTooltip("Card image", tooltip)} ${imgW}&nbsp;&times;&nbsp;${imgH}&nbsp;px</td>
         <td style="padding: 2px; width: 15%; vertical-align: middle; overflow-x: hidden">
           <label id="inp_image_btn" class="btn btn-block btn-default">Image File</label>
           <input type="file" id="inp_image" accept="image/*" style="display: none"/></td>
         <td style="padding: 2px; vertical-align: middle; text-align: left">
           <div id="inp_image_name" style="display: inline-block; vertical-align: middle"></div>
         <td style="padding: 2px; vertical-align: middle; text-align: center">
           <label id="btn_image_discard" class="btn btn-block btn-warning btn-outline btn-padding">Discard</label>
         <td style="padding: 2px; width: 5%; vertical-align: middle">
           <button id="btn_image_copy" class="btn btn-block btn-danger btn-copy">\u27A4</button></td>`;
    }
    tbody.insertRow(-1).innerHTML = `<td style="padding: 1px; background-color: black"
                                         colspan="${smallControls ? 3 : 5}"></td>`;

    $('#inp_image_btn').click(function() { $('#inp_image').click(); });

    $('#inp_image').change(function() {
      var filename = $(this).val().split('\\').pop().split('/').pop();
      if (filename.length > 20) {
        filename = filename.substring(0, 20) + "...";
      }
      $('#inp_image_name').text(filename);

      var cardSide = globIsReckless ? globCardData.reck : globCardData.cons;
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          var scaled = null;
          var image = new Image();
          image.onload = function() {
            if (this.width != imgW || this.height != imgH) {
              scaled = `Image is scaled from ${this.width} x ${this.height} px to ${imgW} x ${imgH} px.`;
            }
            var srccanv = document.createElement('canvas');
            srccanv.width = this.width; srccanv.height = this.height;
            srccanv.getContext("2d").drawImage(this, 0, 0, this.width, this.height);
            var dstcanv = document.createElement('canvas');
            dstcanv.width = imgW; dstcanv.height = imgH;
            picaModule.resize(srccanv, dstcanv).then(function() {
              var newimg = new Image();
              newimg.onload = function() {
                cardSide.image = {name: filename, img: this};
                editorToCard();
                if (scaled) alert(scaled);
              }
              newimg.src = dstcanv.toDataURL('image/jpeg', 1);
            }, function() {});
          };
          image.src = 'data:image/png;base64,' + encode64(new Uint8Array(e.target.result));
        };
        reader.readAsArrayBuffer(this.files[0]);
      }
    });

    $('#btn_image_discard').click(function() {
      $('#inp_image_name').text("No image selected");
      var cardSide = globIsReckless ? globCardData.reck : globCardData.cons;
      cardSide.image = null;
      editorToCard();
    });

    $('#btn_image_copy').click(function() {
      globCardData.reck.image = globCardData.cons.image;
      editorToCard();
    });
  }

  function appendEffectControls(tbody) {

    let lineColspan = (smallControls) ? "1" : "2";
    let divClass = (smallControls) ? "btn-group-vertical btn-block" : "btn-group-full-width";

    let trcopy = tbody.insertRow(-1);
    trcopy.innerHTML =
      `<td style="padding: 2px; vertical-align: middle; text-align: center" colspan="2">
        <label class="btn btn-block btn-danger btn-copy btn-padding" id="btn_effect_copy">\u27A4</label></td>`;
    $(`#btn_effect_copy`).click(function() {
      let props = ["dy", "size", "space", "effect", "special", "text"];
      for (let p of props) globCardData.reck.effect[p] = globCardData.cons.effect[p];
      editorToCard();
    });

    let tr1 = tbody.insertRow(-1);
    tr1.innerHTML =
      `<td style="padding: 2px; vertical-align: middle; text-align: center">${sizesControls("effect")}</td>
       <td style="padding: 2px; vertical-align: middle; text-align: center">
        <div data-toggle="buttons"><div class="btn-group ${divClass}">
          <label class="btn btn-primary btn-outline btn-padding btn-effect-space">
            <input type="radio" name="inp-effect-space" value="normal">Normal line space</input></label>
          <label class="btn btn-primary btn-outline btn-padding btn-effect-space">
            <input type="radio" name="inp-effect-space" value="small">Small line space</input></label>
        </div></div></td>`;
    tr1.onchange = function() {editorToCard(); };

    let tr2 = tbody.insertRow(-1);
    tr2.innerHTML =
      `<td style="padding: 2px; vertical-align: middle; text-align: center">
         ${labelWithTooltip("Block space", tt_block_space)}</td>
       <td style="padding: 2px; vertical-align: middle; text-align: center">
          <div data-toggle="buttons"><div class="btn-group ${divClass}">
            <label class="btn btn-padding btn-primary btn-outline btn-effect-dy">
              <input type="radio" name="inp-effect-dy" value="normal">Normal</input></label>
            <label class="btn btn-padding btn-primary btn-outline btn-effect-dy">
              <input type="radio" name="inp-effect-dy" value="small">Small</input></label>
            <label class="btn btn-padding btn-primary btn-outline btn-effect-dy">
              <input type="radio" name="inp-effect-dy" value="smallest">Smallest</input></label>
          </div></div></td>`;
    tr2.onchange = function() {editorToCard(); };
    tbody.insertRow(-1).innerHTML = '<td style="padding: 1px; background-color: black" colspan="2"></td>';

    var tabindex = 1;
    let itemsList = [["special", "Special", tt_effect_special],
                     ["effect", "Effect", tt_effect_effect],
                     ["text", "Results", tt_effect_text]];
    for (let [key, value, tooltip]  of itemsList) {
      let tre = tbody.insertRow(-1);
      tre.innerHTML =
        `<td style="padding: 2px; width: 25%; vertical-align: middle; tabindex=-1 line-height: 110%">
           ${labelWithTooltip("Subsection&nbsp;" + value, tooltip)}</td>
         <td style="padding: 2px; vertical-align: middle; font-family: monospace">
           <textarea class="form-control text-autosize" id="inp_effect_${key}" rows="1" minrows="1" tabindex=${tabindex}
                     style="resize: none; padding: 2px 6px; line-height: 120%"></textarea></td>`;
      tbody.insertRow(-1).innerHTML = '<td style="padding: 1px; background-color: black" colspan="2"></td>';
      tabindex++;
    }
  }

  appendSelectors();
  appendButtons();

  var tblmeta = tblMeta.appendChild(document.createElement("tbody"));
  appendMeta(tblmeta, "Display name", "display_name", tt_display_name, 1);
  appendMeta(tblmeta, "Source", "source", tt_source, 2);
  appendMeta(tblmeta, "Language&nbsp;code", "lngcode", tt_lngcode, 3);
  appendActionType(tblmeta, smallControls ? nameActionTypeMapSmall : nameActionTypeMap);

  var tblbody = tblMain.appendChild(document.createElement("tbody"));
  appendCtrl(tblbody, "Name", "name", tt_name, 1);
  appendCtrl(tblbody, "Second name", "name_aux", tt_name_aux, 2);
  appendCtrl(tblbody, "Tags", "tags", tt_tags, 3);
  appendCtrl(tblbody, "Skill check", "check", tt_check, 4);
  appendRecharge(tblbody, nameRechargeMap, tt_recharge);
  appendCtrl(tblbody, smallControls ? "Difficulty mod" : "Difficulty modifier", "difficulty", tt_difficulty, 5);
  appendImage(tblbody, tt_image);
  appendCtrl(tblbody, "Requirements", "requirements", tt_requirements, 6);

  var teffectbody = tblEffect.appendChild(document.createElement("tbody"));
  appendEffectControls(teffectbody);

  $('.btn-copy').attr("disabled", globIsReckless);
  $('.btn-copy').toggleClass("disable-a-href", globIsReckless);

  $('textarea').focus(function() {
    activeEditor = this;
    var disable = this.id.match("inp_name|inp_name_aux|inp_tags|inp_check") ? true : false;
    $(".btn-whs").attr("disabled", disable);
    $(".btn-whs").toggleClass("disable-a-href", disable);
  });
  $('textarea').keyup("propertychange", function() {
    if(typeof this.timer !== undefined) { clearTimeout(this.timer); }
    this.timer = setTimeout(editorToCard, 500);
  });
  $('.inp-range').on("input", function() {
    $(`#${this.id}_val`).html(Number(this.value).toFixed(2));
    editorToCard();
  });
  $('.whs-text-dropdown').on("click", "li a", function(event) {
    var x = $(this).text();
    $(this).closest('div').find('.ddn-text').text(x);
    editorToCard();
  });
  $('.btn-sz').click(function() {
    var less = (this.id.substring(0, 10) == "btnsz_less");
    var target = this.id.substring(11);
    var target = $("#txtsz_" + this.id.substring(11));
    var currentval = Number(target.text());
    var sizes = [];
    for (let sz of txtSizes.values()) { sizes.push(sz); }

    var idx = sizes.indexOf(currentval);
    if (less && idx > 0) {
      target.text(sizes[idx-1].toFixed(2));
      editorToCard();
    }
    if (!less && idx < sizes.length) {
      target.text(sizes[idx+1].toFixed(2));
      editorToCard();
    }
    return false;
  });

  autosize($(".text-autosize"));
  autosize.update($(".text-autosize"));
  $("[data-toggle=tooltip]").tooltip()
    .click(function() { var th = $(this);
                        if (th.tooltip('toggle')) {
                          $("[data-toggle=tooltip]").not(th).tooltip('hide');
                        }
                        return false;
                      })
    .mouseleave(function() { $(this).tooltip('hide');
                      });

}

//==================================================================================================
//===================================    TOOLTIPS    ===============================================
//==================================================================================================

function labelWithTooltip(label, tooltip) {

  if (tooltip) {
    return `<a href="#" class="tooltip-wh" data-toggle="tooltip" data-trigger="manual"
               data-html=true data-placement="right" data-original-title="${tooltip}">${label}</a>`;
  } else {
    return label;
  }
}



const tt_display_name =
  "Display name indicates how the card is represented in a list of cards. " +
  "It is not drawn on a card itself.";
const tt_source =
  "Source indicates a code of the card's origin, like<br>" +
  "<b>WHF01</b> for Warhammer Fantasy Roleplay Core Set<br>" +
  "<b>oWHF26</b> for a From The Grave print-on-demand expansion.<br>" +
  "You can use <b>custom</b> or your name for any custom cards.<br><br>" +
  "If the card has several sources, put all of them separated with a space:<br>" +
  "<b>WHF04 WHF10</b> for a 'Grave Blade' card that appears both in The Gathering Storm and " +
  " Creature Vault.<br><br>" +
  "The codes for the components are found in Card &amp; Component Lists document;";
const tt_lngcode =
  "Two symbols language code, like <b>en</b> for Engligh, <b>ru</b> for Russian, " +
  "<b>es</b> for Spanish, and so on.";
const tt_name =
  "Primary name of the card that appears on its top, one line only.";
const tt_name_aux =
  "Secondary name of the card that appears below the primary name. Can be " +
  "a second part of the primary name or can be translation of the name to a target language.";
const tt_tags =
  "Tags of the card separated by a comma and a whitespace.";
const tt_check =
  "What skill is checked and versus what - or 'None' if no check required.";
const tt_difficulty =
  "Additional difficulty of the card, represented with Warhammer dice symbols.";
const tt_requirements =
  "Requirements section of the card - conditions required to use that card.";
const tt_recharge =
  "Recharge icon on the top-right corner of an action card.";
const tt_image =
  `Image to be shown on a card - any image will be scaled to match the expected pixel size.`;
const tt_size =
  "The font size can be scaled up and down. " +
  " Usually you'll want to have smaller font to fit more text."
const tt_block_space =
  "The interval between blocks can be reduced to fit more text. " +
  "It is also possible to reduce line spacing interval, although less recommended.";
const tt_effect_special =
  "The section starting with '<b>Special:</b>'. The first word will be displayed in bold."
const tt_effect_effect =
  "The section starting with '<b>Effect:</b>'. The first word will be displayed in bold."
const tt_effect_text =
  "Main section of the card's effect part. Usually has multiple lines each starting with one of " +
  "<b>[w1]</b>, <b>[w2]</b>, <b>[w3]</b>, <b>[w4]</b> or <b>[w5]</b> sorted in order.";
