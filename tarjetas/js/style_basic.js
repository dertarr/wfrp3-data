// jshint esversion: 6

// This is a basic style description for WH cards drawing
// Parameter "font_factor_x" must be defined as a string, so it is not scaled

const basicActions = {
  dpi: 360,
  width: 810, height: 1260, gap: 0, // 2.25" x 3.50" at 360 dpi
  bgcolor: "#D6D6C2",
  border_radius: 40,
  hbw: 1, // half of the border width

  header_height: 180,
  cons_color: "#10B010", reck_color: "#B01010",
  name: {
    font: "Wfrp3Lib", fontmod: "small-caps bold", fontsize: 48, fontcolor: "white",
    font_factor_x: "0.8", stroke: "black", strokesize: 6,
    x: 120, y: 7, w: 570, h: 48,
    align_x: "center", align_y: "top",
  },
  name_aux: {
    font: "Wfrp3Lib", fontmod: "small-caps bold", fontsize: 58, fontcolor: "white",
    font_factor_x: "0.8", stroke: "black", strokesize: 6,
    x: 120, y: 56, w: 570, h: 58,
    align_x: "center", align_y: "middle",
  },
  tags: {
    font: "Wfrp3Lib", fontmod: "italic", fontsize: 42, fontcolor: "white",
    stroke: "black", strokesize: 6,
    x: 120, y: 134, w: 570, h: 44,
    align_x: "center", align_y: "bottom",
  },
  difficulty: {
    font: "Wfrp3Lib", fontmod: null, fontsize: 58, fontcolor: "black",
    stroke: null,
    whstroke: "white",
    strokesize: 9, extra: 3,
    x: 12, y: 120, w: 786, h: 60,
    align_x: "left", align_y: "bottom",
  },
  type: {
    x: 6, y: 18, w: 120, h: 162,
  },
  recharge: {
    x: 696, y: 18, w: 120, h: 162,
  },

  check_height: 58,
  check: {
    font: "Wfrp3Lib", fontmod: "small-caps bold", fontsize: 42, fontcolor: "white",
    stroke: "black", strokesize: 6,
    x: 12, y: 180, w: 786, h: 58,
    align_x: "center", align_y: "middle",
  },

  effect_offset: 252,
  image: {
    x: 0, y: 0, w: 810, h: 276,
  },
  requirements: {
    font: "Wfrp3Lib", fontsize: 42, fontcolor: "black", interval: 3,
    stroke: null,
    whstroke: "black",
    strokesize: 6,
    x: 15, y: 0, dy: 9, w: 780, h: 1260,
    align_x: "center", align_y: "top",
    background: {x: 8, w: 790, dy: 3, color: "#99BBFF", radius: 15},
    separator: {h: 6, color: "#402010"},
  },
  effect: {
    font: "Wfrp3Lib", fontsize: 42, fontcolor: "black", interval: 3,
    stroke: null,
    whstroke: "black",
    strokesize: 6,
    x: 15, y: 0, w: 780, h: 1260,
    dy: {normal: 10, small: 6, smallest: 4},
    align_x: "left", align_y: "top",
    background: {x: 10, w: 790, dy: 3, color: "#D9B38C", radius: 15},
  },
};
