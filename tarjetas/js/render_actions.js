// jshint esversion: 6
// jshint laxbreak: true

txtSizes = new Set([0.80, 0.85, 0.90, 0.95, 1.00, 1.05, 1.10, 1.15, 1.20]);

function readProperty(obj, key, type, defaultval) {
  return (obj.hasOwnProperty(key) && (typeof obj[key] === type)) ? obj[key] : defaultval;
}

function readOption(jsonData, key, isReckless) {
  var side = (isReckless && jsonData.reckless !== true) ? jsonData.reckless : jsonData.conservative;
  var options = readProperty(side, "options", "object", []);
  for (let opt of options) {

    if (typeof opt === "string" && opt.startsWith(key)) {
      return (opt === key) ? true : opt.substring(key.length+1);
    }
  }
  return null;
}

function getItemByKey(jsonData, itemKey, isReckless) {
  if (jsonData.hasOwnProperty(itemKey)) {
    return jsonData[itemKey];
  }
  var objCons = jsonData.conservative;
  var item = objCons[itemKey] || null;
  if (isReckless) {
    var itemReck = jsonData.reckless[itemKey] || null;
    item = (itemReck === true) ? item : itemReck;
  }
  return item;
}

function getItemByKey_v1_0(jsonData, itemKey, isReckless, defaultval) {
  var side = (isReckless && jsonData.reckless !== true) ? jsonData.reckless : jsonData.conservative;
  return readProperty(side, itemKey, typeof defaultval, defaultval);
}

// This function uses getItemByKey for getting simple text data
function getTextAndSize_Old(jsonData, itemKey, isReckless) {
  var item = getItemByKey(jsonData, itemKey, isReckless);
  var retitem = {text: "", size: 1};
  if (item !== null) {
    retitem.text = (typeof item.text == "string") ? item.text : "";
    retitem.size = item.hasOwnProperty("size") && txtSizes.has(item.size) ? item.size : 1;
    if (item.hasOwnProperty("no_extra_space")) {
      retitem.space = "small";
    }
  }
  return retitem;
}

function getTextAndSize_v1_0(jsonData, itemKey, isReckless) {

  var sz = 1;
  var side = (isReckless && jsonData.reckless !== true) ? jsonData.reckless : jsonData.conservative;
  var text = readProperty(side, itemKey, "string", "");
  var optsz = readOption(jsonData, itemKey + ".size", isReckless);
  if (optsz !== null) {
    let newsz = Number(optsz);
    sz = txtSizes.has(newsz) ? newsz : sz;
  }
  return {text: text, size: sz};
}

// CardActionSide is a subclass for an action side
class CardActionSide {
  constructor(isReckless) {
    this.name = {text: "", size: 1};
    this.name_aux = {text: "", size: 1};
    this.tags = {text: "", size: 1};
    this.recharge = "0";
    this.difficulty = {text: "", size: 1};
    this.check = {text: "", size: 1};
    this.requirements = {text: "", size: 1};
    this.effect = {effect: "", special: "", text: [], size: 1, space: "normal", dy: "normal"};
    this.image = null;
    this.isReckless = isReckless;
  }

  deepcopy() {
    var newSide = new CardActionSide(this.isReckless);
    newSide.recharge = this.recharge;
    let props = ["name", "name_aux", "tags", "difficulty", "check", "requirements"];
    for (let p of props) {
      newSide[p].text = this[p].text;
      newSide[p].size = this[p].size;
    }
    let effectprops = ["effect", "special", "size", "dy", "space"];
    for (let p of effectprops) newSide.effect[p] = this.effect[p];
    for (let txt of this.effect.text) newSide.effect.text.push(txt);
    newSide.image = this.image === null ? null : {name: this.image.name, img: this.image.img};
    return newSide;
  }

  fromJson(jsonData, fmtver) {
    var sltext = ['name', 'name_aux', 'tags', 'difficulty', 'check', 'requirements'];
    for (let itemKey of sltext) {
      if (fmtver === "1.0") {
        this[itemKey] = getTextAndSize_v1_0(jsonData, itemKey, this.isReckless);
      } else {
        this[itemKey] = getTextAndSize_Old(jsonData, itemKey, this.isReckless);
      }
    }
    var effect;
    if (fmtver === "1.0") {
      let recharge = getItemByKey_v1_0(jsonData, "recharge", this.isReckkess, "");
      this.recharge = imgRechargeMap.hasOwnProperty(recharge) ? recharge : "0";
      effect = getItemByKey_v1_0(jsonData, "effect", this.isReckless, {});
      let eff_dy = readOption(jsonData, "effect.dy", this.isReckless);
      this.effect.dy = (eff_dy === "smallest" || eff_dy === "small") ? eff_dy : "normal";
      let eff_space = readOption(jsonData, "effect.space", this.isReckless);
      this.effect.space = (eff_space === "small") ? eff_space : "normal";
      let eff_sz = Number(readOption(jsonData, "effect.size", this.isReckless));
      this.effect.size = txtSizes.has(eff_sz) ? eff_sz : 1;
    } else {
      let recharge = getItemByKey(jsonData, "recharge", this.isReckkess);
      this.recharge = imgRechargeMap.hasOwnProperty(recharge) ? recharge : "0";
      effect = getItemByKey(jsonData, "effect", this.isReckless);
      this.effect.dy = txtSizes.has(effect.dy) ? effect.dy : "normal";
      this.effect.size = txtSizes.has(effect.size) ? effect.size : 1;
      this.effect.space = (effect.no_extra_space) ? "small" : "normal";
    }
    this.effect.effect = readProperty(effect, "effect", "string", "");
    this.effect.special = readProperty(effect, "special", "string", "");
    var efftext = readProperty(effect, "text", "object", []);
    if (Array.isArray(efftext) && (efftext.length < 15)) {
      let good = true;
      for (let txtline of efftext) {
        good = good && (typeof txtline == "string");
      }
      this.effect.text = good ? efftext : [];
    }
  }

  toJSON() {
    let side = {};
    let options = [];

    let simpleItems = ["name", "name_aux", "tags", "difficulty", "check", "requirements"];
    for (let item of simpleItems) {
      if (this[item].text) {
        side[item] = this[item].text;
        if (this[item].size != 1) {
          options.push(item + ".size=" + this[item].size.toFixed(2));
        }
      }
    }
    side.recharge = this.recharge;

    side.effect = {};
    if (this.effect.special) side.effect.special = this.effect.special;
    if (this.effect.effect) side.effect.effect = this.effect.effect;
    if (this.effect.text) side.effect.text = this.effect.text.slice();
    if (this.effect.size != 1) options.push("effect.size=" + this.effect.size.toFixed(2));
    if (this.effect.dy != "normal") options.push("effect.dy=" + this.effect.dy);
    if (this.effect.space) options.push("effect.space=" + this.effect.space);
    if (this.image) side.image = "img_" + this.image.name;
    if (options.length > 0) side.options = options;

    return side;
  }
}

function splitSubstr(str, len) {
  var ret = [ ];
  for (var offset = 0, strLen = str.length; offset < strLen; offset += len) {
    ret.push(str.substr(offset, len));
  }
  return ret;
}

globCardLoadedEvent = null;

// CardAction is a class to work with action cards
class CardAction {

  constructor() {
    this.displayName = "New Card";
    this.lngcode = "ru";
    this.type = "support";
    this.source = "WHF01",
    this.cons = new CardActionSide(false);
    this.reck = new CardActionSide(true);
    this.onload = new Event('card-data-load');
  }

  deepcopy() {
    var newCard = new CardAction();
    newCard.type = this.type;
    newCard.source = this.source;
    newCard.lngcode = this.lngcode;
    newCard.displayName = this.displayName;
    newCard.cons = this.cons.deepcopy();
    newCard.reck = this.reck.deepcopy();
    if (this.imgcons) newCard.imgcons = this.imgcons.slice(0);
    if (this.imgreck) newCard.imgreck = this.imgreck.slice(0);
    return newCard;
  }

  // Import action card data from a parsed JSON file
  fromJson(jsonData) {

    var fmtver = readProperty(jsonData, "fmtversion", "string", null);

    this.lngcode = (typeof jsonData.lngcode == "string") ? jsonData.lngcode : "en";
    this.cons.fromJson(jsonData, fmtver);
    this.reck.fromJson(jsonData, fmtver);
    this.source = (fmtver === "1.0") ? jsonData.source : this.cons.source;
    this.displayName = (fmtver === "1.0") ? jsonData.display_name : this.cons.name.text;

    this.type = imgActionTypeMap.hasOwnProperty(jsonData.type) ? jsonData.type : jsonData.cons.type;
    var item = this;
    var imgcons = new Image();
    var imgreck = new Image();
    this.cons.image = null;
    this.reck.image = null;
    if (fmtver === "1.0") {
      function imagesLoaded(item) {
        if (imgcons.src)
          item.cons.image = {name: jsonData.conservative.image, img: imgcons};
        if (imgcons.src && imgreck.src === imgcons.src)
          item.reck.image = {name: jsonData.conservative.image, img: imgcons};
        if (imgreck.src && imgreck.src !== imgcons.src)
          item.reck.image = {name: jsonData.reckless.image, img: imgreck};
        document.dispatchEvent(globCardLoadedEvent);
      }
      function loadReck(item) {
        if (jsonData.reckless === true || jsonData.conservative.image === jsonData.reckless.image) {
          imgreck = imgcons;
          imagesLoaded(item);
        } else if (jsonData.reckless.image) {
          imgreck.onload = function() { imagesLoaded(item); };
          imgreck.src = jsonData[jsonData.reckless.image].join('');
        } else {
          imagesLoaded(item);
        }
      }
      if (jsonData.conservative.image) {
        imgcons.onload = function() { loadReck(item); };
        imgcons.src = jsonData[jsonData.conservative.image].join('');
      } else {
        loadReck(item);
      }
    } else {
      function imagesLoaded(item) {
        if (jsonData.conservative.image && jsonData.conservative.image.link == "imgcons")
          item.cons.image = {name: jsonData.conservative.image.name, img: imgcons};
        if (jsonData.reckless.image && jsonData.reckless.image.link == "imgcons")
          item.reck.image = {name: jsonData.reckless.image.name, img: imgcons};
        if (jsonData.reckless.image && jsonData.reckless.image.link == "imgreck")
          item.reck.image = {name: jsonData.reckless.image.name, img: imgreck};
        document.dispatchEvent(globCardLoadedEvent);
      }
      function loadReck(item) {
        if (jsonData.imgreck) {
          imgreck.onload = function() { imagesLoaded(item); };
          imgreck.src = jsonData.imgreck.join('');
        } else {
          imagesLoaded(item);
        }
      }
      if (jsonData.imgcons) {
        imgcons.onload = function() { loadReck(item); };
        imgcons.src = jsonData.imgcons.join('');
      } else {
        loadReck(item);
      };
    }

  }

  // Make a filename for the card (without extension)
  genFileName() {
    var dname = this.displayName.toLowerCase().replace(/ /g,"_");
    return dname + "_" + this.lngcode;
  }

  toJSON() {
    let dump = {fmtversion: "1.0", display_name: this.displayName,
                source: this.source, lngcode: this.lngcode};

    dump.type = this.type;
    dump.conservative = this.cons;
    dump.reckless = (JSON.stringify(this.cons) === JSON.stringify(this.reck)) ? true : this.reck;

    if (this.cons.image) {
      let imgname = "img_" + this.cons.image.name;
      dump[imgname] = splitSubstr(this.cons.image.img.src, 128);
    }
    if (this.reck.image && (!this.cons.image || this.reck.image.name != this.cons.image.name)) {
      let imgname = "img_" + this.reck.image.name;
      dump[imgname] = splitSubstr(this.reck.image.img.src, 128);
    }

    return JSON.stringify(dump, null, 2);
  }
}

function repositionMainLook(redraw=true) {

  function position(div, x, y, w, h) {
    div.style.left = x + "px";
    div.style.top = y + "px";
    div.style.width = w + "px";
    div.style.height = h + "px";
    div.style.position = "absolute";
  }

  var leftDiv = document.getElementById("sidenav");
  var masterDiv = document.getElementById("master_div");
  var canvasDiv = document.getElementById("canvas_div");
  var belowControls = document.getElementById("below_canvas_controls");
  var belowDiv = document.getElementById("below_canvas_div");
  var editorDiv = document.getElementById("editor_master");
  var headerDiv = document.getElementById("holy_grail_header");
  var middleDiv = document.getElementById("holy_grail_middle");
  var footerDiv = document.getElementById("holy_grail_footer");
  var totalW = $(window).outerWidth();
  var totalH = $(window).outerHeight();
  var htow = globActionCardStyle.height / (2 * globActionCardStyle.width + globActionCardStyle.gap);
  var realH = totalH - 40 - 80 - 40; // twice 40px for header and footer, and 80px for below canvas
  var realW = realH / htow;

  // If max width required is more that available width, then reduce height of drawing canvas
  // and increase height of the space below canvas.
  // Otherwise adjust divs to the left and to the right of the canvas
  if (realW > (totalW * 0.55)) {
    realW = Math.round(totalW * 0.55);
    realH = Math.round(realW * htow);
  }

  // Half of the space height below the canvas, excluding header and footer
  var belowHalf = (totalH - realH - 40 - 40) / 2;

  // Adjust sizes of horizontal panels
  var leftW = Math.round((totalW - realW) * 0.25);

  position(headerDiv, 0, 0, totalW, 40);
  position(middleDiv, 0, 40, totalW, totalH - 80);
  position(footerDiv, 0, totalH - 40, totalW, 40);
  position(leftDiv, 0, 0, leftW, totalH - 80);
  position(masterDiv, leftW, 0, realW, totalH - 80);
  position(editorDiv, leftW + realW, 0, totalW - leftW - realW, totalH - 80);
  position(canvasDiv,     0, 0,                 realW, realH);
  position(belowControls, 0, realH,             realW, belowHalf);
  position(belowDiv,      0, realH + belowHalf, realW, belowHalf);

  var canvDst = document.getElementById("wh_canvas");
  canvDst.width = realW; canvDst.height = realH;
  if (redraw) {
    picaModule.resize(canv360, canvDst);
  }
  createEditorControls(false);
}

// loadActionCard loads and renders an action card from a json source file.
// This is the function that should be called outside of the script here.
function renderWarhammerCard(canvDst, cardData) {

  var ctx360 = canv360.getContext("2d");
  ctx360.fillStyle = "white";
  ctx360.fillRect(0, 0, canv360.width, canv360.height);

  drawCardSide(ctx360, false);
  ctx360.save();
  ctx360.translate(globActionCardStyle.width + globActionCardStyle.gap, 0);
  drawCardSide(ctx360, true);
  ctx360.restore();

  if (canvDst !== null) {
    picaModule.resize(canv360, canvDst);
  }
}

function drawEmptyCard() {

  globCardData = new CardAction();
  globCardDataSaved = globCardData.deepcopy();
  var whCanvas = document.getElementById("wh_canvas");
  renderWarhammerCard(whCanvas, globCardData);
  cardToEditor();
  return false;
}

function jsonToCardAction(jsonText) {

  var jsonData;
  try {
    jsonData = JSON.parse(jsonText);
  } catch(e) {
    alert(e);
  }

  var whCanvas = document.getElementById("wh_canvas");
  globCardData = new CardAction();
  if (globCardLoadedEvent === null) {
    globCardLoadedEvent = new Event('card-data-load');
    document.addEventListener("card-data-load", function() {
      globCardDataSaved = globCardData.deepcopy();
      renderWarhammerCard(whCanvas, globCardData);
      cardToEditor();
    });
  }
  globCardData.fromJson(jsonData);
}

function convertPath(urlPath) {
  var path = "#";
  var items = split('/');
  if ((items.length > 2) && (items[1] == "actions")) {
    if (items[0] == "ru") {
      return "cards/core_ru/actions/" + items[2] + "_ru";
    } else if (items[0] == "dertarr_ru") {
      return "cards/dertarr_ru/actions/" + items[2] + "_ru";
    } else if (items[0] == "dertarr") {
      return "cards/dertarr/actions/" + items[2];
    }

  }

  return "#";
}
