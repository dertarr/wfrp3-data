# Warhammer Fantasy Roleplay 3ed web toolkit

# WFRP 3ed symbols
The WFRP 3ed Card Editor is using a modified Liberation font family. With the modified fonts installed you may easily type any WFRP 3ed symbols in Windows. The colors are embedded into the font, so you would not need to select it every time you type.

## MS Word or Libre Office
All WFRP 3ed symbols are hidden on the 500-535 hex range. So, type a code (like "501" for example, without quotes), then press Alt+X, and the code will be converted to WFRP 3ed symbol - provided you selected the correct font to type with. You may press Alt+x again, and the symbol will be converted back to hex code.

## Adobe Photoshop
Things are somewhat more tricky with Adobe products. In Photoshop, open the "Glyphs" tab and navigate to the symbols. You may select the "Cyrillic" filter in the select box so that you would not have to scroll too much. Just be sure not to use the 900 range under the "Devanagari" tab as that might cause your lines to shrink or expand.

## Code Symbols
| Code |      Symbol |
|------|-------------|
|  500 | Challenge (crossed swords) |
|  501 | Success (hammer)   |
|  502 | Boon (eagle) |
|  503 | Sigmar's comet |
|  504 | Bane (skull) |
|  505 | Chaos star |
|  506 | White d6 (fortune) die |
|  507 | Black d6 (misfortune) die |
|  508 | Purple d8 (challenge) die |
|  509 | Blue d8 (characteristics) die |
|  510 | Green d10 (conservative stance) die |
|  511 | Red d10 (reckless stance) die |
|  512 | Torn boot (symbolizes fatigue) |
|  513 | Spiked diadem (symbolizes stress) |
|  514 | Extortion (found on reckless dice) |
|  515 | Three lacerated scars (symbolizes a wound) |
|  516 | Yellow d6 (training) die |
|  517 | Red d6 (aggression) die |
|  518 | Blue d6 (cunning) die |
|  519 | Righteous success (a hammer with a plus beneath it) |
|  520 | Delay (found on conservative dice |
|  521 | Conservative stance puzzle tile |
|  522 | Neutral stance puzzle tile |
|  523 | Reckless stance puzzle tile |
|  524 | A blood drop symbol (don't use to express neither extorion nor wound) |

# Versions
## v0.10
A lot of changes, including several dozens of ready to use cards (in Russian), an option to open a card by url parameters, new font and so on.

## v0.07
A template for a New Card is opened by default instead of Troll-Feller Strike.
New format of action card JSON encoding - to prepare for a client-server application.
Tooltips for editor lables to comment on what is expected for each textarea.
Common section of editor.

## v0.06
Improved card rendering quality (mostly on screen rather then on downloadable image) by utilizing pica library to resize images.
Improved UI on smaller screens.

## v0.05
Import of JSON files is supported. Adding a card image is supported.
A little bit better quality of text rendering for requirements and effect sections.
There are plans to move rendering to SVG which should improve rendering quality a lot.

## v0.04
Card editor is functional with export and import JSON file. Still need support for card images.
Some bugs are expected. Cards are rendered differently on Linux and Windows systems due to unreliable "measureText" function.

## v0.03
Major rework to render the cards graphically on a canvas rather then by HTML text. Still need some effort to make text rendering quality perfect.
Added link to render the card at 300 dpi and download the result to a JPEG file.
Prepared some stuff for the card editor.

## v0.02
Takes URL arguemnt to locate a json source file to render.

## v0.01
Able to render a Troll-Feller Strike card out of source json file.

# Plans for development
A final destination for the project is to create a web toolkit to assist Game Masters in their role. The toolkit should be able to create NPC or monsters and manage encounters with those. That includes tracking initiative, actions recharging, spending A/C/E pool, gathering wounds and criticals, and so on.
Another part is a system to create all types of cards, so that Game Masters may easily create their own action cards, locations, and so on. This is to some extend can already be achieved with Strange Eons software, but the renderer there isn't perfect, and the tool itself may be not easy to use.

## Engagement goals
 - Create unit test system and cover all existing code with it.
 - Create an editor that would be used to generate action cards in json format.
 - Add support of navigation through cards directory.

## Close range goals
 - Create complete set (sans and serif, italic, bold, italic-bold) of WH fonts out of Liberation font family.
 - Move from javascript to something more convenient like golang.
 - Make editors auto-filling with json data from existing cards.

## Medium range goals
 - Take care of permissions of files.
 - Make sure the web toolkit looks good with any screen resolution.

## Long range goals
 - Create support for creature cards, condition cards, location cards, talent cards and so on.
 - Create support for various card styles.
 - Create a database project to store all the cards (instead of them being stored as json files).

## Extreme range goals
 - Create an NPC/monster generator editor.
 - Adapt web page for using with mobile devices.
 - Create a script to convert existing cards in Strange Eons to our source json format.

# Few words about the author
I have some 20+ years of development experience, mostly with C and VHDL. But this project uses HTML and Javascript - which are pretty new to me. So please don't be too harsh on me. But if you have some advice or comments about any part of the project (except for legal notes) please share. You may contact me directly with an e-mail to epikur (at) inbox (point) ru.

# If you want to contribute
I'll be very happy. Suggested option to contribute is to fork the repository, change things in your fork and then make pull requests.

# Legal note and copyright
This is an open source project distributed under GNU license: http://www.gnu.org/licenses/gpl-3.0.en.html.
No part of this repository may be used for any commersial purpose without approval of the author (Dertarr) and major contributors, and may Sigmar save you if you break it! Witch Hunters are already preparing a warming stake for you, so consider yourself warned!!!

Dertarr
